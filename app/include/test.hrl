%%% Helper macro to tests.
%%%
%%% @author Olivier Pirson [http://www.opimedia.be/]
%%% @version April 22, 2018

%% Print a message to mention that a test is not covered.
-define(NOT_COVERED, FunctionName_ = hd(io_lib:format("~w", [?FUNCTION_NAME])),
        ct:print("Function ~s() is NOT covered!~n",
                 [string:sub_string(FunctionName_, 1, length(FunctionName_) - 6)])).
