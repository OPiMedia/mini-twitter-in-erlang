%%% @author Olivier Pirson [http://www.opimedia.be/]
%%% @version May 10, 2018

%% Number of server actors to receive message by users.
-define(NB_DATA_ACTOR, erlang:system_info(schedulers)).


%% Number the tweets by page.
%% (Must be >= 3, essentially to make correct tests.)
-define(PAGE_SIZE, 10).


%% Delay between each send_recent_tweets message.
-define(TIMER_DELAY, 1000 div ?NB_DATA_ACTOR).
