%%% Helper macros to help during development.
%%%
%%% @author Olivier Pirson [http://www.opimedia.be/]
%%% @version April 21, 2018

-define(PRINT(X), ct:print("PRINT ~s ~w~n", [??X, X])).


-define(PRINTB(X), ct:print("PRINTB ~B~n", [X])).

-define(PRINTS(X), ct:print("PRINTS ~s~n", [X])).

-define(PRINTW(X), ct:print("PRINTW ~w~n", [X])).


-define(PRINTSB(S, X), ct:print("PRINTSB ~s ~B~n", [S, X])).

-define(PRINTSS(S, X), ct:print("PRINTSS ~s ~s~n", [S, X])).

-define(PRINTSW(S, X), ct:print("PRINTSW ~s ~w~n", [S, X])).
