%%% @author Olivier Pirson [http://www.opimedia.be/]
%%% @version April 21, 2018

%% Recommendation:
%% run each test at least 30 times to get statistically relevant results.
-define(NB_REPEAT, 30).
