mkdir -p benchmarks

#??? for i in {1..64}
for i in {1..3}
do
    echo "> timeline, $i threads"
    # erl +S $i -noshell -s benchmark__server_centralized test_timeline -s init stop > benchmarks/server_centralized__timeline-$i.txt
    erl +S $i -noshell -s benchmark__server_centralized_map test_timeline -s init stop > benchmarks/server_centralized_map__timeline-$i.txt
    echo "-----"

    echo "> tweet, $i threads"
    # erl +S $i -noshell -s benchmark__server_centralized test_tweet -s init stop > benchmarks/server_centralized__tweet-$i.txt
    erl +S $i -noshell -s benchmark__server_centralized_map test_tweet -s init stop > benchmarks/server_centralized_map__tweet-$i.txt
    echo "=========="
done
