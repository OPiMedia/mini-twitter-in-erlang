%%% @doc Unit tests for data.erl.
%%%
%%% @author Olivier Pirson [http://www.opimedia.be/]
%%% @copyright GPLv3 --- 2018 Olivier Pirson
%%% @version May 9, 2018

-module(test__data).

-include_lib("config.hrl").
-include_lib("debug.hrl").
-include_lib("test.hrl").

-include_lib("eunit/include/eunit.hrl").


%%
%% Helpers
%%

add_users(Data, 0) ->
    ?assert(data:is_data_t(Data)),
    Data;
add_users(Data, Nb) ->
    ?assert(data:is_data_t(Data)),
    ?assert(is_integer(Nb)),
    ?assert(Nb >= 1),
    {NewData, NewUserId} = data:add_new_user(Data),
    ?assertEqual(NewUserId + 1, length( NewData)),
    add_users(NewData, Nb - 1).



%%
%% Tests
%%

starting_msg_info_test() ->
    ct:print("START tests in ~s~n", [?FILE]).



add_new_user__test() ->
    Data = data:create(),

    {New0Data, New0UserId} = data:add_new_user(Data),
    ?assertEqual(0, New0UserId),
    ?assertEqual([data:empty_value(New0UserId)], New0Data),

    {New1Data, New1UserId} = data:add_new_user(New0Data),
    ?assertEqual(1, New1UserId),
    ?assertEqual([data:empty_value(New0UserId),
                  data:empty_value(New1UserId)], New1Data),

    {New2Data, New2UserId} = data:add_new_user(New1Data),
    ?assertEqual(2, New2UserId),
    ?assertEqual([data:empty_value(New0UserId),
                  data:empty_value(New1UserId),
                  data:empty_value(New2UserId)], New2Data),
    ?assertEqual(add_users(data:create(), 3), New2Data).


add_subscription_to_user__test() ->
    Data = add_users(data:create(), 3),
    ?assertEqual(Data, data:add_subscription_to_user(Data, 1, 1)),

    New0Data = data:add_subscription_to_user(Data, 1, 2),
    ?assertEqual([data:empty_value(0),
                  {user, 1, [], sets:from_list([2])},
                  data:empty_value(2)], New0Data),
    ?assertEqual(New0Data, data:add_subscription_to_user(New0Data, 1, 2)),
    ?assertEqual(New0Data, data:add_subscription_to_user(New0Data, 1, 1)),

    New1Data = data:add_subscription_to_user(New0Data, 1, 0),
    ?assertEqual([data:empty_value(0),
                  {user, 1, [], sets:from_list([0, 2])},
                  data:empty_value(2)], New1Data),

    New2Data = data:add_subscription_to_user(New1Data, 2, 1),
    ?assertEqual([data:empty_value(0),
                  {user, 1, [], sets:from_list([0, 2])},
                  {user, 2, [], sets:from_list([1])}], New2Data),
    ?assertEqual(New2Data, data:add_subscription_to_user(New2Data, 0, 0)).


add_tweet__test() ->
    ?NOT_COVERED.


create__test() ->
    ?assertEqual([], data:create()).


empty_value__test() ->
    ?assertEqual({user, 0, [], sets:new()}, data:empty_value(0)),
    ?assertEqual({user, 42, [], sets:new()}, data:empty_value(42)).


get_timeline__test() ->
    ?NOT_COVERED.


get_tweets__test() ->
    ?NOT_COVERED.


is_data_t__test() ->
    ?assert(data:is_data_t([])),
    ?assert(data:is_data_t(data:create())),

    ?assertNot(data:is_data_t(maps:new())),
    ?assertNot(data:is_data_t([42])),
    ?assertNot(data:is_data_t(42)).
