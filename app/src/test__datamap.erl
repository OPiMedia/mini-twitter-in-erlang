%%% @doc Unit tests for datamap.erl.
%%%
%%% @author Olivier Pirson [http://www.opimedia.be/]
%%% @copyright GPLv3 --- 2018 Olivier Pirson
%%% @version May 15, 2018

-module(test__datamap).

-include_lib("config.hrl").
-include_lib("debug.hrl").
-include_lib("test.hrl").

-include_lib("eunit/include/eunit.hrl").

-export([get_timeline_all/2,
         pagerefs_length/1]).


%%
%% Helpers
%%

add_subscriptions(DataMap, []) ->
    ?assert(datamap:is_datamap_t(DataMap)),
    DataMap;
add_subscriptions(DataMap, [{UserId, UserIdToSubscribeTo} | RemainSubscriptions]) ->
    ?assert(datamap:is_datamap_t(DataMap)),
    ?assert(common:is_user_id_t(UserId)),
    ?assert(maps:is_key(UserId, DataMap)),
    ?assert(common:is_user_id_t(UserIdToSubscribeTo)),
    add_subscriptions(datamap:add_subscription_to_user(DataMap, UserId, UserIdToSubscribeTo),
                      RemainSubscriptions).


add_tweets(DataMap, UserId, TweetContentPrefix, Nb) ->
    ?assert(datamap:is_datamap_t(DataMap)),
    ?assert(common:is_user_id_t(UserId)),
    ?assert(io_lib:char_list(TweetContentPrefix)),
    ?assert(is_integer(Nb)),
    ?assert(Nb >= 1),
    add_tweets_loop_(DataMap, UserId, TweetContentPrefix, Nb, 0).

add_tweets_loop_(DataMap, _UserId, _TweetContentPrefix, 0, _I) ->
    ?assert(datamap:is_datamap_t(DataMap)),
    DataMap;
add_tweets_loop_(DataMap, UserId, TweetContentPrefix, Nb, I) ->
    ?assert(datamap:is_datamap_t(DataMap)),
    ?assert(common:is_user_id_t(UserId)),
    ?assert(maps:is_key(UserId, DataMap)),
    ?assert(io_lib:char_list(TweetContentPrefix)),
    ?assert(is_integer(Nb)),
    ?assert(Nb >= 1),
    ?assert(is_integer(I)),
    ?assert(I >= 0),
    {{Tweets, LengthTweets,
      PageRefs},
     Subscriptions} = maps:get(UserId, DataMap),
    ?assertEqual(LengthTweets, length(Tweets)),
    ?assertEqual(pagerefs_length(LengthTweets), length(PageRefs)),
    ?assertEqual(datamap:build_pagerefs(Tweets, LengthTweets), PageRefs),
    {NewDataMap, NewTweet} =
        datamap:add_tweet(DataMap, UserId,
                          lists:flatten(
                            io_lib:format("~s~B", [TweetContentPrefix, I]))),
    {{NewTweets, NewLengthTweets,
      NewPageRefs},
     NewSubscriptions} = maps:get(UserId, NewDataMap),
    ?assertEqual(NewTweet, hd(NewTweets)),
    ?assertEqual(NewLengthTweets, length(NewTweets)),
    ?assertEqual(pagerefs_length(NewLengthTweets), length(NewPageRefs)),
    ?assertEqual(datamap:build_pagerefs(NewTweets, NewLengthTweets), NewPageRefs),
    ?assertEqual(LengthTweets + 1, NewLengthTweets),
    if
        NewLengthTweets rem ?PAGE_SIZE == 0 ->
            ?assertEqual(length(PageRefs) + 1, length(NewPageRefs));
        true ->
            ?assertEqual(length(PageRefs), length(NewPageRefs))
    end,
    ?assertEqual(Subscriptions, NewSubscriptions),
    add_tweets_loop_(NewDataMap, UserId, TweetContentPrefix, Nb - 1, I + 1).


add_users(DataMap, 0) ->
    ?assert(datamap:is_datamap_t(DataMap)),
    DataMap;
add_users(DataMap, Nb) ->
    ?assert(datamap:is_datamap_t(DataMap)),
    ?assert(is_integer(Nb)),
    ?assert(Nb >= 1),
    {NewDataMap, NewUserId} = datamap:add_new_user(DataMap),
    ?assert(maps:is_key(NewUserId, NewDataMap)),
    add_users(NewDataMap, Nb - 1).


add_users_tweets(DataMap, []) ->
    ?assert(datamap:is_datamap_t(DataMap)),
    DataMap;
add_users_tweets(DataMap, [NbTweet | RemainNbTweet]) ->
    ?assert(datamap:is_datamap_t(DataMap)),
    ?assert(is_integer(NbTweet)),
    ?assert(NbTweet >= 0),
    {NewUserDataMap, NewUserId} = datamap:add_new_user(DataMap),
    NewTweetsDataMap =
        if
            NbTweet >= 1 ->
                add_tweets(NewUserDataMap, NewUserId,
                           lists:flatten(
                             io_lib:format("Tweet from user ~B: ", [NewUserId])),
                           NbTweet);
            true ->
                NewUserDataMap
        end,
    add_users_tweets(NewTweetsDataMap, RemainNbTweet).


%% @doc Returns the complete timeline of the given user
%% (all its tweets and all tweets of all followed users).
%% UserId must be valid user id.
-spec get_timeline_all(datamap:datamap_t(), common:user_id_t()) -> common:tweets_t().
get_timeline_all(DataMap, UserId) ->
    ?assert(datamap:is_datamap_t(DataMap)),
    ?assert(common:is_user_id_t(UserId)),
    ?assert(maps:is_key(UserId, DataMap)),
    {{Tweets, LengthTweets,
      _PageRefs},
     Subscriptions} = maps:get(UserId, DataMap),
    ?assert(length(Tweets) == LengthTweets),
    AllTweets = lists:foldl(fun(OtherUserId, Acc) ->
                                    ?assert(OtherUserId >= 0),
                                    ?assertNotEqual(UserId, OtherUserId),
                                    {{OtherTweets, OtherLengthTweets,
                                      _OtherPageRefs},
                                     _OtherSubscriptions} = maps:get(OtherUserId, DataMap,
                                                                     datamap:empty_value()),
                                    ?assert(length(OtherTweets) == OtherLengthTweets),
                                    lists:append(OtherTweets, Acc)
                            end,
                            Tweets,
                            Subscriptions),
    lists:sort(fun common:is_ordonned_tweet/2, AllTweets).


%% @doc Returns the correct length of a pagerefs
%% corresponding to the number of tweets given by LengthTweets.
-spec pagerefs_length(non_neg_integer()) -> non_neg_integer().
pagerefs_length(LengthTweets) ->
    ?assert(is_integer(LengthTweets)),
    ?assert(LengthTweets >= 0),
    (LengthTweets - (LengthTweets rem ?PAGE_SIZE)) div ?PAGE_SIZE.



%%
%% Tests
%%

starting_msg_info_test() ->
    ct:print("START tests in ~s~n", [?FILE]).



add_new_user__test() ->
    DataMap = datamap:create(),

    {New0DataMap, New0UserId} = datamap:add_new_user(DataMap),
    ?assertEqual(0, New0UserId),
    ?assertEqual(#{0 => datamap:empty_value()}, New0DataMap),

    {New1DataMap, New1UserId} = datamap:add_new_user(New0DataMap),
    ?assertEqual(1, New1UserId),
    ?assertEqual(#{0 => datamap:empty_value(),
                   1 => datamap:empty_value()}, New1DataMap),

    {New2DataMap, New2UserId} = datamap:add_new_user(New1DataMap),
    ?assertEqual(2, New2UserId),
    ?assertEqual(#{0 => datamap:empty_value(),
                   1 => datamap:empty_value(),
                   2 => datamap:empty_value()}, New2DataMap),
    ?assertEqual(add_users(datamap:create(), 3), New2DataMap).


add_subscription_to_user__test() ->
    DataMap = add_users(datamap:create(), 3),
    ?assertEqual(DataMap, datamap:add_subscription_to_user(DataMap, 1, 1)),

    New0DataMap = datamap:add_subscription_to_user(DataMap, 1, 2),
    ?assertEqual(#{0 => datamap:empty_value(),
                   1 => {{[], 0,
                          []},
                         [2]},
                   2 => datamap:empty_value()}, New0DataMap),
    ?assertEqual(New0DataMap, datamap:add_subscription_to_user(New0DataMap, 1, 2)),
    ?assertEqual(New0DataMap, datamap:add_subscription_to_user(New0DataMap, 1, 1)),

    New1DataMap = datamap:add_subscription_to_user(New0DataMap, 1, 0),
    ?assertEqual(#{0 => datamap:empty_value(),
                   1 => {{[], 0,
                          []},
                         [0, 2]},
                   2 => datamap:empty_value()}, New1DataMap),

    New2DataMap = datamap:add_subscription_to_user(New1DataMap, 2, 1),
    ?assertEqual(#{0 => datamap:empty_value(),
                   1 => {{[], 0,
                          []},
                         [0, 2]},
                   2 => {{[], 0,
                          []},
                         [1]}}, New2DataMap),
    ?assertEqual(New2DataMap, datamap:add_subscription_to_user(New2DataMap, 0, 0)),

    %% Follows invalid user.
    New3DataMap = datamap:add_subscription_to_user(New2DataMap, 1, 1001),
    ?assertEqual(#{0 => datamap:empty_value(),
                   1 => {{[], 0,
                          []},
                         [1001, 0, 2]},
                   2 => {{[], 0,
                          []},
                         [1]}}, New3DataMap).


add_tweet__test() ->
    DataMap = add_users(datamap:create(), 3),
    Timestamp = common:current_time(),

    {New0DataMap, New0Tweet} = datamap:add_tweet(DataMap, 1, "A"),
    {tweet, 1, New0Timestamp, "A"} = New0Tweet,
    ?assert(New0Timestamp > Timestamp),
    ?assertEqual(#{0 => datamap:empty_value(),
                   1 => {{[New0Tweet], 1,
                          []},
                         []},
                   2 => datamap:empty_value()}, New0DataMap),

    {New1DataMap, New1Tweet} = datamap:add_tweet(New0DataMap, 0, "B"),
    {tweet, 0, New1Timestamp, "B"} = New1Tweet,
    ?assert(New1Timestamp > New0Timestamp),
    ?assertEqual(#{0 => {{[New1Tweet], 1,
                          []},
                         []},
                   1 => {{[New0Tweet], 1,
                          []},
                         []},
                   2 => datamap:empty_value()}, New1DataMap),

    {New2DataMap, New2Tweet} = datamap:add_tweet(New1DataMap, 1, "C"),
    {tweet, 1, New2Timestamp, "C"} = New2Tweet,
    ?assert(New2Timestamp > New1Timestamp),
    ?assertEqual(#{0 => {{[New1Tweet], 1,
                          []},
                         []},
                   1 => {{[New2Tweet, New0Tweet], 2,
                          []},
                         []},
                   2 => datamap:empty_value()}, New2DataMap),

    New3DataMap = add_tweets(New2DataMap, 0, "Content", ?PAGE_SIZE*10),
    {{New3Tweets, ?PAGE_SIZE*10 + 1,
      PageRefs},
     []} = maps:get(0, New3DataMap),
    ?assertEqual(?PAGE_SIZE*10 + 1, length(New3Tweets)),
    ?assertEqual(10, length(PageRefs)).


add_tweet__page__test() ->
    DataMap = add_tweets(add_users(datamap:create(), 3),
                         1, "Tweet of user 1 ", ?PAGE_SIZE - 1),

    ?assertEqual(maps:get(0, DataMap), datamap:empty_value()),
    ?assertEqual(maps:get(2, DataMap), datamap:empty_value()),
    {{Tweets, ?PAGE_SIZE - 1,
      []},
     []} = maps:get(1, DataMap),
    ?assertEqual(?PAGE_SIZE - 1, length(Tweets)),

    {New0DataMap, New0Tweet} = datamap:add_tweet(DataMap, 1, "Added 1"),
    ?assertEqual(maps:get(0, New0DataMap), datamap:empty_value()),
    ?assertEqual(maps:get(2, New0DataMap), datamap:empty_value()),
    {{New0Tweets, ?PAGE_SIZE,
      [New0Tweets]},
     []} = maps:get(1, New0DataMap),
    ?assertEqual(?PAGE_SIZE, length(New0Tweets)),
    ?assertEqual([New0Tweet | Tweets], New0Tweets),

    {New1DataMap, New1Tweet} = datamap:add_tweet(New0DataMap, 1, "Added 2"),
    ?assertEqual(maps:get(0, New1DataMap), datamap:empty_value()),
    ?assertEqual(maps:get(2, New1DataMap), datamap:empty_value()),
    {{New1Tweets, ?PAGE_SIZE + 1,
      [New0Tweets]},
     []} = maps:get(1, New1DataMap),
    ?assertEqual(?PAGE_SIZE + 1, length(New1Tweets)),
    ?assertEqual([New1Tweet | New0Tweets], New1Tweets),

    New2DataMap = add_tweets(New1DataMap,
                             1, "Tweet of user 1, second step ", ?PAGE_SIZE - 2),
    ?assertEqual(maps:get(0, New2DataMap), datamap:empty_value()),
    ?assertEqual(maps:get(2, New2DataMap), datamap:empty_value()),
    {{New2Tweets, ?PAGE_SIZE*2 - 1,
      [New0Tweets]},
     []} = maps:get(1, New2DataMap),
    ?assertEqual(?PAGE_SIZE*2 - 1, length(New2Tweets)),

    {New3DataMap, New3Tweet} = datamap:add_tweet(New2DataMap, 1, "Added 3"),
    ?assertEqual(maps:get(0, New3DataMap), datamap:empty_value()),
    ?assertEqual(maps:get(2, New3DataMap), datamap:empty_value()),
    {{New3Tweets, ?PAGE_SIZE*2,
      [New3Tweets, New0Tweets]},
     []} = maps:get(1, New3DataMap),
    ?assertEqual(?PAGE_SIZE*2, length(New3Tweets)),
    ?assertEqual([New3Tweet | New2Tweets], New3Tweets),

    {New4DataMap, New4Tweet} = datamap:add_tweet(New3DataMap, 1, "Added 4"),
    ?assertEqual(maps:get(0, New4DataMap), datamap:empty_value()),
    ?assertEqual(maps:get(2, New4DataMap), datamap:empty_value()),
    {{New4Tweets, ?PAGE_SIZE*2 + 1,
      [New3Tweets, New0Tweets]},
     []} = maps:get(1, New4DataMap),
    ?assertEqual(?PAGE_SIZE*2 + 1, length(New4Tweets)),
    ?assertEqual([New4Tweet | New3Tweets], New4Tweets).


add_user__test() ->
    DataMap = datamap:create(),

    New0DataMap = datamap:add_user(DataMap, 42),
    ?assertEqual(#{42 => datamap:empty_value()}, New0DataMap),

    New1DataMap = datamap:add_user(New0DataMap, 666),
    ?assertEqual(#{42 => datamap:empty_value(),
                   666 => datamap:empty_value()}, New1DataMap),

    New2DataMap = datamap:add_user(New1DataMap, 36),
    ?assertEqual(#{42 => datamap:empty_value(),
                   666 => datamap:empty_value(),
                   36 => datamap:empty_value()}, New2DataMap).


build_pagerefs__test() ->
    {timeout, 60,
     fun() ->
             lists:foreach(fun(I) ->
                                   DataMap = add_tweets(add_users(datamap:create(), 3),
                                                        0, "Tweet of user 0 ", I),
                                   ?assertEqual(maps:get(1, DataMap), datamap:empty_value()),
                                   ?assertEqual(maps:get(2, DataMap), datamap:empty_value()),
                                   {{Tweets, I,
                                     PageRefs},
                                    []} = maps:get(0, DataMap),
                                   ?assertEqual(I, length(Tweets)),
                                   ?assertEqual(pagerefs_length(I), length(PageRefs)),
                                   ?assertEqual(datamap:build_pagerefs(Tweets, I), PageRefs)
                           end,
                           lists:seq(1, ?PAGE_SIZE*10))
     end}.


create__test() ->
    ?assertEqual(#{}, datamap:create()).


empty_value__test() ->
    ?assertEqual({{[], 0,
                   []},
                  []}, datamap:empty_value()).


get_subscriptions__test() ->
    Nbs = lists:seq(0, 13),
    DataMap = add_users_tweets(datamap:create(), Nbs),
    lists:foreach(
      fun(UserId) ->
              ?assertEqual([], datamap:get_subscriptions(DataMap, UserId))
      end,
      lists:seq(0, length(Nbs) - 1)),

    NewDataMap = add_subscriptions(DataMap,
                                   [{0, 0},
                                    {1, 10},
                                    {3, 5}, {3, 3},
                                    {8, 4}, {8, 2}, {8, 12}, {8, 7},
                                    {5, 3}, {5, 9}]),

    lists:foreach(
      fun(UserId) ->
              MustBeEmpty = not lists:member(UserId, [1, 3, 5, 8]),
              if
                  MustBeEmpty ->
                      ?assertEqual([], datamap:get_subscriptions(NewDataMap, UserId));
                  true -> ok
              end
      end,
      lists:seq(0, length(Nbs) - 1)),

    ?assertEqual([10], datamap:get_subscriptions(NewDataMap, 1)),
    ?assertEqual([5], datamap:get_subscriptions(NewDataMap, 3)),
    ?assertEqual([9, 3], datamap:get_subscriptions(NewDataMap, 5)),
    ?assertEqual([7, 12, 2, 4], datamap:get_subscriptions(NewDataMap, 8)).


get_timeline__without_following__test() ->
    {timeout, 60,
     fun() ->
             Nbs = [7, 0, 5, 1, 2, 3, 4,
                    ?PAGE_SIZE   - 1, ?PAGE_SIZE,   ?PAGE_SIZE   + 1,
                    ?PAGE_SIZE*2 - 1, ?PAGE_SIZE*2, ?PAGE_SIZE*2 + 1,
                    ?PAGE_SIZE*3 - 1, ?PAGE_SIZE*3, ?PAGE_SIZE*3 + 1,
                    ?PAGE_SIZE*4 - 1, ?PAGE_SIZE*4, ?PAGE_SIZE*4 + 1,
                    ?PAGE_SIZE*5 - 1, ?PAGE_SIZE*5, ?PAGE_SIZE*5 + 1,
                    ?PAGE_SIZE*6 - 1, ?PAGE_SIZE*6, ?PAGE_SIZE*6 + 1],
             DataMap = add_users_tweets(datamap:create(), Nbs),
             ?assertEqual(lists:seq(0, length(Nbs) - 1), lists:sort(maps:keys(DataMap))),

             lists:foreach(
               fun(I) ->
                       Nb = lists:nth(I, Nbs),
                       {{AllTweets, LengthAllTweets,
                         _PageRefs},
                        _Subscriptions} = maps:get(I - 1, DataMap),
                       ?assertEqual(length(AllTweets), LengthAllTweets),
                       ?assertEqual(Nb, LengthAllTweets),

                       lists:foreach(
                         fun(Page) ->
                                 Tweets = datamap:get_timeline(DataMap, I - 1, Page),
                                 PageNb = min(?PAGE_SIZE,
                                              max(0,
                                                  Nb - ?PAGE_SIZE * (Page - 1))),
                                 ?assertEqual(PageNb, length(Tweets)),
                                 ?assertEqual(lists:sort(fun common:is_ordonned_tweet/2, Tweets),
                                              Tweets),

                                 CorrectTweets = try
                                                     lists:sublist(AllTweets,
                                                                   ?PAGE_SIZE * (Page - 1) + 1,
                                                                   ?PAGE_SIZE)
                                                 catch
                                                     _Exception:_Reason -> []
                                                 end,
                                 ?assertEqual(CorrectTweets, Tweets),

                                 ?assertEqual(datamap:get_tweets(DataMap, I - 1, Page), Tweets)
                         end,
                         lists:seq(1, 10))
               end,
               lists:seq(1, length(Nbs)))
     end}.


get_timeline__with_following__test() ->
    {timeout, 60,
     fun() ->
             Nbs = [7, 0, 5, 1, 2, 3, 4,
                    ?PAGE_SIZE   - 1, ?PAGE_SIZE,   ?PAGE_SIZE   + 1,
                    ?PAGE_SIZE*2 - 1, ?PAGE_SIZE*2, ?PAGE_SIZE*2 + 1,
                    ?PAGE_SIZE*3 - 1, ?PAGE_SIZE*3, ?PAGE_SIZE*3 + 1,
                    ?PAGE_SIZE*4 - 1, ?PAGE_SIZE*4, ?PAGE_SIZE*4 + 1,
                    ?PAGE_SIZE*5 - 1, ?PAGE_SIZE*5, ?PAGE_SIZE*5 + 1,
                    ?PAGE_SIZE*6 - 1, ?PAGE_SIZE*6, ?PAGE_SIZE*6 + 1],
             DataMap = add_subscriptions(add_users_tweets(datamap:create(), Nbs),
                                         [{0, 0},
                                          {1, 10},
                                          {2, 3}, {2, 1001},
                                          {3, 2}, {3, 3}, {3, 4}, {3, 1002},
                                          {15, 17}, {15, 19}, {15, 14},
                                          {16, 17}, {16, 19}, {16, 14}]),
             ?assertEqual(lists:seq(0, length(Nbs) - 1), lists:sort(maps:keys(DataMap))),

             lists:foreach(
               fun(I) ->
                       Nb = lists:nth(I, Nbs),
                       AllTweets = get_timeline_all(DataMap, I - 1),
                       LengthAllTweets = length(AllTweets),
                       ?assert(Nb =< LengthAllTweets),

                       lists:foreach(
                         fun(Page) ->
                                 Tweets = datamap:get_timeline(DataMap, I - 1, Page),
                                 PageNb = min(?PAGE_SIZE,
                                              max(0,
                                                  LengthAllTweets - ?PAGE_SIZE * (Page - 1))),
                                 ?assert(PageNb == length(Tweets)),
                                 ?assertEqual(lists:sort(fun common:is_ordonned_tweet/2, Tweets),
                                              Tweets),

                                 CorrectTweets
                                     = try
                                           lists:sublist(AllTweets, ?PAGE_SIZE * (Page - 1) + 1,
                                                         ?PAGE_SIZE)
                                       catch
                                           _Exception:_Reason -> []
                                       end,
                                 ?assertEqual(CorrectTweets, Tweets),

                                 Subscriptions = datamap:get_subscriptions(DataMap, I - 1),
                                 case Subscriptions of
                                     [] -> ?assertEqual(datamap:get_tweets(DataMap, I - 1, Page),
                                                        Tweets);
                                     _ -> ok
                                 end
                         end,
                         lists:seq(1, 10))
               end,
               lists:seq(1, length(Nbs)))
     end}.


get_tweets__test() ->
    {timeout, 60,
     fun() ->
             Nbs = [7, 0, 5, 1, 2, 3, 4,
                    ?PAGE_SIZE   - 1, ?PAGE_SIZE,   ?PAGE_SIZE   + 1,
                    ?PAGE_SIZE*2 - 1, ?PAGE_SIZE*2, ?PAGE_SIZE*2 + 1,
                    ?PAGE_SIZE*3 - 1, ?PAGE_SIZE*3, ?PAGE_SIZE*3 + 1,
                    ?PAGE_SIZE*4 - 1, ?PAGE_SIZE*4, ?PAGE_SIZE*4 + 1,
                    ?PAGE_SIZE*5 - 1, ?PAGE_SIZE*5, ?PAGE_SIZE*5 + 1,
                    ?PAGE_SIZE*6 - 1, ?PAGE_SIZE*6, ?PAGE_SIZE*6 + 1],
             DataMap = add_users_tweets(datamap:create(), Nbs),
             ?assertEqual(lists:seq(0, length(Nbs) - 1), lists:sort(maps:keys(DataMap))),

             lists:foreach(
               fun(I) ->
                       Nb = lists:nth(I, Nbs),
                       {{AllTweets, LengthAllTweets,
                         _PageRefs},
                        _Subscriptions} = maps:get(I - 1, DataMap),
                       ?assertEqual(length(AllTweets), LengthAllTweets),
                       ?assertEqual(Nb, LengthAllTweets),

                       lists:foreach(
                         fun(Page) ->
                                 Tweets = datamap:get_tweets(DataMap, I - 1, Page),
                                 PageNb = min(?PAGE_SIZE,
                                              max(0,
                                                  Nb - ?PAGE_SIZE * (Page - 1))),
                                 ?assertEqual(PageNb, length(Tweets)),
                                 ?assertEqual(lists:sort(fun common:is_ordonned_tweet/2, Tweets),
                                              Tweets),

                                 CorrectTweets = try
                                                     lists:sublist(AllTweets,
                                                                   ?PAGE_SIZE * (Page - 1) + 1,
                                                                   ?PAGE_SIZE)
                                                 catch
                                                     _Exception:_Reason -> []
                                                 end,
                                 ?assertEqual(CorrectTweets, Tweets)
                         end,
                         lists:seq(1, 10))
               end,
               lists:seq(1, length(Nbs)))
     end}.


is_datamap_t__test() ->
    ?assert(datamap:is_datamap_t(maps:new())),
    ?assert(datamap:is_datamap_t(datamap:create())),
    ?assert(datamap:is_datamap_t(add_users_tweets(datamap:create(), lists:seq(0, 42)))),

    ?assertNot(datamap:is_datamap_t([])),
    ?assertNot(datamap:is_datamap_t([42])),
    ?assertNot(datamap:is_datamap_t(42)).
