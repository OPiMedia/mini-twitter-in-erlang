%%% @doc Unit tests for server_multi_map.erl.
%%%
%%% @author Olivier Pirson [http://www.opimedia.be/]
%%% @copyright GPLv3 --- 2018 Olivier Pirson
%%% @version May 9, 2018

-module(test__server_multi_map).

-include_lib("debug.hrl").

-include_lib("eunit/include/eunit.hrl").



msg_info_test() ->
    ct:print("START tests in ~s~n", [?FILE]).



initialization_test() ->
    catch unregister(user_id_actor_name),
    server_multi_map:initialize().


visible_tweets_test() ->
    ResponsePids = init_for_test(),
    UserIds = lists:seq(0, 7),
    %% Add tweets by each user
    lists:foreach(fun({UserId, I}) ->
                          TweetContent = lists:flatten(
                                           io_lib:format("Tweet no. ~B by user ~B.", [I, UserId])),
                          server:tweet(server_multi_map:i_to_data_actor_pid(UserId, ResponsePids),
                                       UserId, TweetContent)
                  end, [{UserId, I}
                        || UserId <- UserIds, I <- lists:seq(0, 119)]),
    %% User 3 follows user 0 and 2
    server:subscribe(server_multi_map:i_to_data_actor_pid(3, ResponsePids), 3, 0),
    server:subscribe(server_multi_map:i_to_data_actor_pid(3, ResponsePids), 3, 2),

    {ok, OutputTweets} = file:open("tests/test__server_multi_map__tweets.txt", [write]),

    lists:foreach(
      fun(Page) ->
              file:write(OutputTweets, io_lib:format("~nTweets for each user, page ~B~n",
                                                     [Page])),
              file:write(
                OutputTweets,
                string:join(
                  lists:map(fun(UserId) ->
                                    lists:foldl(fun(Tweet, Acc) ->
                                                        string:concat(Acc, common:tweet_to_string(Tweet))
                                                end,
                                                "",
                                                server:get_tweets(
                                                  server_multi_map:i_to_data_actor_pid(UserId, ResponsePids),
                                                  UserId, Page))
                            end,
                            UserIds),
                  "\n"))
      end, lists:seq(1, 2)).

-ifdef(zzz). % ???
    {ok, OutputTimeline} = file:open("tests/test__server_multi_map__timeline.txt", [write]),

    lists:foreach(
      fun(Page) ->
              file:write(OutputTimeline, io_lib:format("~nTimeline for each user, page ~B~n",
                                                       [Page])),
              file:write(
                OutputTimeline,
                string:join(
                  lists:map(fun(UserId) ->
                                    lists:foldl(fun(Tweet, Acc) ->
                                                        string:concat(Acc, common:tweet_to_string(Tweet))
                                                end,
                                                "",
                                                server:get_timeline(
                                                  server_multi_map:i_to_data_actor_pid(UserId, ResponsePids),
                                                  UserId, Page))
                            end,
                            UserIds),
                  "\n"))
      end, lists:seq(1, 4)),
    ok.
-endif.


register_user__test() ->
    ServerPids = initialization_test(),
    NbPid = length(ServerPids),
    lists:foreach(fun(I) ->
                          ?assertMatch({I, _ResponsePid},
                                       server:register_user(
                                         server_multi_map:i_to_data_actor_pid(I, ServerPids)))
                  end,
                  lists:seq(0, NbPid - 1)).


init_for_test() ->
    ServerPids = initialization_test(),
    NbPid = length(ServerPids),
    lists:map(fun(I) ->
                      {I, ResponsePid} =
                          server:register_user(
                            server_multi_map:i_to_data_actor_pid(I, ServerPids)),
                      ResponsePid
              end,
              lists:seq(0, NbPid - 1)).


-ifdef(zzz). % ???
get_timeline__without_following__test() ->
    ResponsePids = init_for_test(),

    ?assertMatch([],
                 server:get_timeline(server_multi_map:i_to_data_actor_pid(1, ResponsePids), 1, 1)),
    ?assertMatch([],
                 server:get_timeline(server_multi_map:i_to_data_actor_pid(2, ResponsePids), 2, 1)),

    lists:foreach(fun({ResponsePid, UserId}) ->
                          ?assertMatch([], server:get_timeline(ResponsePid, UserId, 1))
                  end, [{ResponsePid, UserId}
                        || ResponsePid <- ResponsePids, UserId <- lists:seq(0, 3)]).


get_tweets__test() ->
    ResponsePids = init_for_test(),

    ?assertMatch([],
                 server:get_tweets(server_multi_map:i_to_data_actor_pid(1, ResponsePids), 1, 1)),
    ?assertMatch([],
                 server:get_tweets(server_multi_map:i_to_data_actor_pid(2, ResponsePids), 2, 1)),

    lists:foreach(fun({ResponsePid, UserId}) ->
                          ?assertMatch([], server:get_tweets(ResponsePid, UserId, 1))
                  end, [{ResponsePid, UserId}
                        || ResponsePid <- ResponsePids, UserId <- lists:seq(0, 3)]).


get_timeline__with_following__test() ->
    ResponsePids = init_for_test(),

    ?assertMatch([],
                 server:get_timeline(server_multi_map:i_to_data_actor_pid(1, ResponsePids), 1, 1)),
    ?assertMatch([],
                 server:get_timeline(server_multi_map:i_to_data_actor_pid(2, ResponsePids), 2, 1)),

    Before = common:current_time(),
    ?assertMatch(Timestamp when Before < Timestamp,
                                server:tweet(server_multi_map:i_to_data_actor_pid(1, ResponsePids),
                                             1, "Tweet no. 1")),
    After = common:current_time(),

    ?assertMatch([{tweet, 1, Timestamp, "Tweet no. 1"}]
                 when (Before < Timestamp) andalso (Timestamp < After),
                      server:get_tweets(server_multi_map:i_to_data_actor_pid(1, ResponsePids),
                                        1, 1)),
    ?assertMatch([],
                 server:get_tweets(server_multi_map:i_to_data_actor_pid(1, ResponsePids), 2, 1)),

    ?assertMatch([{tweet, 1, Timestamp, "Tweet no. 1"}]
                 when (Before < Timestamp) andalso (Timestamp < After),
                      server:get_timeline(server_multi_map:i_to_data_actor_pid(1, ResponsePids),
                                          1, 1)), % own tweets included in timeline
    ?assertMatch([],
                 server:get_timeline(server_multi_map:i_to_data_actor_pid(2, ResponsePids), 2, 1)),

    ResponsePids. % no subscription


subscribe__test() ->
    ResponsePids = get_timeline__with_following__test(),

    ?assertEqual(ok,
                 server:subscribe(server_multi_map:i_to_data_actor_pid(2, ResponsePids), 2, 1)),

    test:sleep(),
    ?assertMatch([{tweet, 1, _Timestamp, "Tweet no. 1"}], % now there is a subscription
                 server:get_timeline(server_multi_map:i_to_data_actor_pid(2, ResponsePids), 2, 1)),

    server:tweet(server_multi_map:i_to_data_actor_pid(2, ResponsePids), 2, "Tweet no. 2"),
    ?assertMatch([{tweet, 2, Timestamp2, "Tweet no. 2"},
                  {tweet, 1, Timestamp1, "Tweet no. 1"}]
                 when (Timestamp2 > Timestamp1),
                      server:get_timeline(server_multi_map:i_to_data_actor_pid(2, ResponsePids),
                                          2, 1)),
    done.
-endif.
