%%% @doc Common helper functions for tests.
%%%
%%% @author Olivier Pirson [http://www.opimedia.be/]
%%% @version May 14, 2018
-module(test).

-include_lib("stdlib/include/assert.hrl").

-include_lib("config.hrl").

-export([get_timeline_all/2,
         get_tweets_all/2,
         sleep/0,
         two_1234_tweets/3,
         two_tweet/4]).



%% @doc Returns the list of all tweets (for all pages) in the timeline for the given user.
-spec get_timeline_all(pid(), common:user_id_t()) -> common:tweets_t().
get_timeline_all(ResponsePid, UserId) ->
    ?assert(is_pid(ResponsePid)),
    ?assert(common:is_user_id_t(UserId)),
    Tweets = get_timeline_all_loop_(ResponsePid, UserId, 1, []),
    ?assertEqual(lists:sort(fun common:is_ordonned_tweet/2, Tweets), Tweets),
    Tweets.

get_timeline_all_loop_(ResponsePid, UserId, Page, AllTweets) ->
    ?assert(is_pid(ResponsePid)),
    ?assert(common:is_user_id_t(UserId)),
    ?assert(common:is_page_t(Page)),
    ?assert(is_list(AllTweets)),
    PageTweets = server:get_timeline(ResponsePid, UserId, Page),
    lists:foreach(fun(Tweet) ->
                          ?assert(common:is_tweet_t(Tweet)),
                          {tweet, _UserId, Timestamp, TweetContent} = Tweet,
                          ?assert(common:is_tweet_time_t(Timestamp)),
                          ?assert(common:is_tweet_content_t(TweetContent))
                  end,
                  PageTweets),
    if
        PageTweets == [] ->
            AllTweets;
        true ->
            get_timeline_all_loop_(ResponsePid, UserId, Page + 1,
                                   lists:append(AllTweets, PageTweets))
    end.


%% @doc Returns the list of all tweets (for all pages) for the given user.
-spec get_tweets_all(pid(), common:user_id_t()) -> common:tweets_t().
get_tweets_all(ResponsePid, UserId) ->
    ?assert(is_pid(ResponsePid)),
    ?assert(common:is_user_id_t(UserId)),
    Tweets = get_tweets_all_loop_(ResponsePid, UserId, 1, []),
    ?assertEqual(lists:sort(fun common:is_ordonned_tweet/2, Tweets), Tweets),
    Tweets.

get_tweets_all_loop_(ResponsePid, UserId, Page, AllTweets) ->
    ?assert(is_pid(ResponsePid)),
    ?assert(common:is_user_id_t(UserId)),
    ?assert(common:is_page_t(Page)),
    ?assert(is_list(AllTweets)),
    PageTweets = server:get_tweets(ResponsePid, UserId, Page),
    lists:foreach(fun(Tweet) ->
                          ?assert(common:is_tweet_t(Tweet)),
                          {tweet, UserId, Timestamp, TweetContent} = Tweet,
                          ?assert(common:is_tweet_time_t(Timestamp)),
                          ?assert(common:is_tweet_content_t(TweetContent))
                  end,
                  PageTweets),
    if
        PageTweets == [] ->
            AllTweets;
        true ->
            get_tweets_all_loop_(ResponsePid, UserId, Page + 1,
                                 lists:append(AllTweets, PageTweets))
    end.


%% @doc Waits enough to have a complete transmission of information in multi server.
-spec sleep() -> ok.
sleep() ->
    timer:sleep(?TIMER_DELAY*(?NB_DATA_ACTOR + 1)).


%% @doc Returns a pair of list of 1234 tweets (generated one pair after another).
-spec two_1234_tweets(common:user_id_t(), common:user_id_t(), common:tweet_content_t()) -> {common:tweets_t(), common:tweets_t()}.
two_1234_tweets(UserIdA, UserIdB, TweetContentSuffix) ->
    ?assert(common:is_user_id_t(UserIdA)),
    ?assert(common:is_user_id_t(UserIdB)),
    ?assert(io_lib:char_list(TweetContentSuffix)),
    Nb = 1234,
    lists:unzip(
      lists:reverse(lists:map(fun(I) ->
                                      two_tweet(
                                        UserIdA, UserIdB,
                                        lists:flatten(
                                          io_lib:format("Tweet ~B/~B A:~s", [I, Nb,
                                                                             TweetContentSuffix])),
                                        lists:flatten(
                                          io_lib:format("Tweet ~B/~B B:~s", [I, Nb,
                                                                             TweetContentSuffix])))
                              end,
                              lists:seq(1, Nb)))).


%% @doc Returns a pair of tweets (A created before B).
-spec two_tweet(common:user_id_t(), common:user_id_t(), common:tweet_content_t(), common:tweet_content_t()) -> {common:tweet_t(), common:tweet_t()}.
two_tweet(UserIdA, UserIdB, TweetContentA, TweetContentB) ->
    ?assert(common:is_user_id_t(UserIdA)),
    ?assert(common:is_user_id_t(UserIdB)),
    ?assert(common:is_tweet_content_t(TweetContentA)),
    ?assert(common:is_tweet_content_t(TweetContentB)),
    TimestampA = common:current_time(),
    TimestampB = common:current_time(),
    {{tweet, UserIdA, TimestampA, TweetContentA},
     {tweet, UserIdB, TimestampB, TweetContentB}}.
