%%% @doc Public API used to interact with the implementations of the microblogging service.
%%%
%%% The interface is design to be synchronous:
%%% it waits for the reply of the system.
%%%
%%% @author Janwillem Swalens [http://soft.vub.ac.be/soft/members/jswalens]
%%% @author Olivier Pirson [http://www.opimedia.be/]
%%% @copyright GPLv3 --- 2018 Olivier Pirson
%%% @version May 9, 2018

-module(server).

-include_lib("stdlib/include/assert.hrl").

-export([get_timeline/3,
         get_tweets/3,
         register_user/1,
         subscribe/3,
         tweet/3]).



%% @doc Requests to the server
%% a page of the timeline (personal tweets and tweets of followed users) of the user.
%% and returns the corresponding list of tweets.
%% ServerPid must be a server data_actor.
%% For some implementations UserId must be valid user id.
-spec get_timeline(pid(), common:user_id_t(), common:page_t()) -> common:tweets_t().
get_timeline(ServerPid, UserId, Page) ->
    ?assert(is_pid(ServerPid)),
    ?assert(common:is_user_id_t(UserId)),
    ?assert(common:is_page_t(Page)),
    ServerPid ! {self(), get_timeline, UserId, Page},
    receive
        Timeline -> Timeline
    end.


%% @doc Requests to the server
%% a page of the personal tweets of the user
%% and returns the corresponding list of tweets.
%% ServerPid must be a server data_actor.
%% For some implementations UserId must be valid user id.
-spec get_tweets(pid(), common:user_id_t(), common:page_t()) -> common:tweets_t().
get_tweets(ServerPid, UserId, Page) ->
    ?assert(is_pid(ServerPid)),
    ?assert(common:is_user_id_t(UserId)),
    ?assert(common:is_page_t(Page)),
    ServerPid ! {self(), get_tweets, UserId, Page},
    receive
        Tweets -> Tweets
    end.


%% @doc Registers a new user
%% and returns its id and a pid that should be used for subsequent requests by this client.
%% ServerPid must be a server data_actor.
-spec register_user(pid()) -> {common:user_id_t(), pid()}.
register_user(ServerPid) ->
    ?assert(is_pid(ServerPid)),
    ServerPid ! {self(), register_user},
    receive
        UserId_ResponsePid -> UserId_ResponsePid
    end.


%% @doc Subscribes (follows) another user.
%% ServerPid must be a server data_actor.
%% For some implementations, UserId and UserIdToSubscribeTo must be valid user id.
-spec subscribe(pid(), common:user_id_t(), common:user_id_t()) -> ok.
subscribe(ServerPid, UserId, UserIdToSubscribeTo) ->
    ?assert(is_pid(ServerPid)),
    ?assert(common:is_user_id_t(UserId)),
    ?assert(common:is_user_id_t(UserIdToSubscribeTo)),
    ServerPid ! {self(), subscribe, UserId, UserIdToSubscribeTo},
    receive
        ok -> ok
    end.


%% @doc Submits a tweet for a user
%% and returns its timestamp.
%% ServerPid must be a server data_actor.
%% For some implementations UserId must be valid user id.
-spec tweet(pid(), common:user_id_t(), common:tweet_content_t()) -> common:tweet_time_t().
tweet(ServerPid, UserId, TweetContent) ->
    ?assert(is_pid(ServerPid)),
    ?assert(common:is_user_id_t(UserId)),
    ?assert(common:is_tweet_content_t(TweetContent)),
    ServerPid ! {self(), tweet, UserId, TweetContent},
    receive
        Timestamp -> Timestamp
    end.
