%%% @doc Common types and helper functions.
%%%
%%% @author Olivier Pirson [http://www.opimedia.be/]
%%% @copyright GPLv3 --- 2018 Olivier Pirson
%%% @version May 6, 2018

%%%
%%% GPLv3
%%% ------
%%% Copyright (C) 2018 Olivier Pirson
%%%
%%% This program is free software: you can redistribute it and/or modify
%%% it under the terms of the GNU General Public License as published by
%%% the Free Software Foundation, either version 3 of the License, or
%%% (at your option) any later version.
%%%
%%% This program is distributed in the hope that it will be useful,
%%% but WITHOUT ANY WARRANTY; without even the implied warranty of
%%% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
%%% GNU General Public License for more details.
%%%
%%% You should have received a copy of the GNU General Public License
%%% along with this program. If not, see <http://www.gnu.org/licenses/>.
%%%

-module(common).

-include_lib("stdlib/include/assert.hrl").

-include("config.hrl").
-include("debug.hrl").

-export([current_time/0,
         get_tweet_time/1,
         is_ordonned_tweet/2,
         is_page_t/1,
         is_tweet_t/1,
         is_tweet_t/2,
         is_tweets_t/1,
         is_tweets_t/2,
         is_tweet_time_t/1,
         is_tweet_content_t/1,
         is_user_id_t/1,
         list_page/2,
         mod_get/3,
         page_to_enough_nb/1,
         page_to_nb_skip/1,
         sorted_tweets_limited_merge/2,
         sorted_tweets_limited_merge/3,
         sorted_tweets_limited_merge_list/2,
         sorted_tweets_merge/2,
         timestamp_to_datetime_string/1,
         tweet_to_string/1,
         tweets_to_string/1]).

-export_type([page_t/0,
              tweet_t/0,
              tweets_t/0,
              tweet_time_t/0,
              tweet_content_t/0,
              user_id_t/0]).


%%
%% Common types.
%%

%% @type page_t(). Index of a page: 1, 2, 3...
-type page_t() :: pos_integer().

%% @type tweet_t(). Tweet data structure with all information.
-type tweet_t() :: {tweet, user_id_t(), tweet_time_t(), tweet_content_t()}.

%% @type tweets_t(). List of tweet_t().
-type tweets_t() :: [tweet_t()].

%% @type tweet_time_t(). Date/time (in fact timestamp) of a tweet.
-type tweet_time_t() :: erlang:system_time().

%% @type tweet_content_t(). Content text of a tweet (must be not empty).
-type tweet_content_t() :: string().

%% @type user_id_t(). Id of user: 0, 1, 2...
-type user_id_t() :: non_neg_integer().



%%
%% Common helper functions.
%%

%% @doc Returns the current time, used by tweet datas.
-spec current_time() -> tweet_time_t().
current_time() -> erlang:system_time().


%% @doc Returns the Date/time (in fact timestamp) of the tweet.
-spec get_tweet_time(tweet_t()) -> tweet_time_t().
get_tweet_time(Tweet) ->
    ?assert(is_tweet_t(Tweet)),
    {_tweet, _UserId, Timestamp, _TweetContent} = Tweet,
    Timestamp.


%% @doc Returns true if TweetA >= TweetB (by comparing timestamp).
-spec is_ordonned_tweet(tweet_t(), tweet_t()) -> boolean().
is_ordonned_tweet(TweetA, TweetB) ->
    ?assert(is_tweet_t(TweetA)),
    ?assert(is_tweet_t(TweetB)),
    {_user, _UserIdA, TimestampA, _TweetContentA} = TweetA,
    {_user, _UserIdB, TimestampB, _TweetContentB} = TweetB,
    TimestampA >= TimestampB.


%% @doc Returns true iff X is a valid page_t.
-spec is_page_t(any()) -> boolean().
is_page_t(X) ->
     is_integer(X) andalso (X >= 1).


%% @doc Returns true iff X is a valid tweet_t.
-spec is_tweet_t(any()) -> boolean().
is_tweet_t(X) ->
    try
        {tweet, UserId, Timestamp, TweetContent} = X,
        is_user_id_t(UserId)
            andalso is_tweet_time_t(Timestamp)
            andalso is_tweet_content_t(TweetContent)
    catch
        _Exception:_Reason -> false
    end.


%% @doc Returns true iff X is a valid tweet_t of the given user.
-spec is_tweet_t(any(), user_id_t()) -> boolean().
is_tweet_t(X, UserId) ->
    try
        {tweet, UserId, Timestamp, TweetContent} = X,
        is_user_id_t(UserId)
            andalso is_tweet_time_t(Timestamp)
            andalso is_tweet_content_t(TweetContent)
    catch
        _Exception:_Reason -> false
    end.


%% @doc Returns true iff X is a valid tweets_t.
-spec is_tweets_t(any()) -> boolean().
is_tweets_t(X) ->
    is_list(X) andalso lists:all(fun is_tweet_t/1, X).


%% @doc Returns true iff X is a valid tweets_t of the given user.
-spec is_tweets_t(any(), user_id_t()) -> boolean().
is_tweets_t(X, UserId) ->
    is_list(X)
        andalso is_user_id_t(UserId)
        andalso is_list(X)
        andalso lists:all(fun(Tweet) ->
                                  is_tweet_t(Tweet, UserId)
                          end,
                          X).


%% @doc Returns true iff X is a valid tweet_time_t.
-spec is_tweet_time_t(any()) -> boolean().
is_tweet_time_t(X) ->
     is_integer(X) andalso (X >= 0).


%% @doc Returns true iff X is a valid tweet_content_t.
-spec is_tweet_content_t(any()) -> boolean().
is_tweet_content_t(X) ->
    io_lib:char_list(X) andalso (X /= "").


%% @doc Returns true iff X is a valid user_id_t.
-spec is_user_id_t(any()) -> boolean().
is_user_id_t(X) ->
     is_integer(X) andalso (X >= 0).


%% @doc Returns the sublist corresponding to the given page
%% (?PAGE_SIZE determines the size of each complete page).
-spec list_page([T], page_t()) -> [T].
list_page(List, Page) ->
    ?assert(is_list(List)),
    ?assert(is_page_t(Page)),
    try
        lists:sublist(List, page_to_nb_skip(Page) + 1, ?PAGE_SIZE)
    catch
        _Exception:_Reason -> []
    end.


%% @doc Returns the (N%Length + 1)th element of the not empty list List,
%% where Length is the length of List.
-spec mod_get(non_neg_integer(), [T], pos_integer()) -> T.
mod_get(N, List, Length) ->
    ?assert(is_integer(N)),
    ?assert(N >= 0),
    ?assert(is_list(List)),
    ?assertNotEqual([], List),
    ?assert(is_integer(Length)),
    ?assert(Length >= 1),
    ?assertEqual(length(List), Length),
    lists:nth((N rem Length) + 1, List).


%% @doc Returns the number of items from the first item of the first page
%% until the last item (included) of the given page.
-spec page_to_enough_nb(page_t()) -> pos_integer().
page_to_enough_nb(Page) ->
    ?assert(is_page_t(Page)),
    ?PAGE_SIZE * Page.


%% @doc Returns the number of items that must be skipped before the first item of the given page.
-spec page_to_nb_skip(page_t()) -> non_neg_integer().
page_to_nb_skip(Page) ->
    ?assert(is_page_t(Page)),
    (Page - 1) * ?PAGE_SIZE.


%% @doc Returns the Nb more recent tweets from TweetsA and TweetsB, sorted in decreasing order.
%% TweetsA and TweetsB must be also sorted in decreasing order.
-spec sorted_tweets_limited_merge({tweets_t(), tweets_t()}, non_neg_integer()) -> tweets_t().
sorted_tweets_limited_merge({TweetsA, TweetsB}, Nb) ->
    ?assert(is_tweets_t(TweetsA)),
    ?assert(is_tweets_t(TweetsB)),
    ?assert(is_integer(Nb)),
    ?assert(Nb >= 0),
    sorted_tweets_limited_merge_loop_(TweetsA, TweetsB, Nb, []).

%% @doc Returns the Nb more recent tweets from TweetsA and TweetsB, sorted in decreasing order.
%% TweetsA and TweetsB must be also sorted in decreasing order.
-spec sorted_tweets_limited_merge(tweets_t(), tweets_t(), non_neg_integer()) -> tweets_t().
sorted_tweets_limited_merge(TweetsA, TweetsB, Nb) ->
    ?assert(is_tweets_t(TweetsA)),
    ?assert(is_tweets_t(TweetsB)),
    ?assert(is_integer(Nb)),
    ?assert(Nb >= 0),
    sorted_tweets_limited_merge_loop_(TweetsA, TweetsB, Nb, []).

-spec sorted_tweets_limited_merge_loop_(tweets_t(), tweets_t(), non_neg_integer(), tweets_t()) -> tweets_t().
sorted_tweets_limited_merge_loop_(_TweetsA, _TweetsB, 0, ReversedResultTweets) ->
    lists:reverse(ReversedResultTweets);
sorted_tweets_limited_merge_loop_([], TweetsB, Nb, ReversedResultTweets) ->
    ?assert(Nb >= 0),
    lists:reverse(ReversedResultTweets) ++ lists:sublist(TweetsB, Nb);
sorted_tweets_limited_merge_loop_(TweetsA, [], Nb, ReversedResultTweets) ->
    ?assert(Nb >= 0),
    lists:reverse(ReversedResultTweets) ++ lists:sublist(TweetsA, Nb);
sorted_tweets_limited_merge_loop_(TweetsA, TweetsB, Nb, ReversedResultTweets) ->
    ?assert(Nb >= 0),
    [TweetA | RemainTweetsA] = TweetsA,
    [TweetB | RemainTweetsB] = TweetsB,
    IsOrdonned = is_ordonned_tweet(TweetA, TweetB),
    if
        IsOrdonned ->
            sorted_tweets_limited_merge_loop_(RemainTweetsA, TweetsB, Nb - 1,
                                      [TweetA | ReversedResultTweets]);
        true ->
            sorted_tweets_limited_merge_loop_(TweetsA, RemainTweetsB, Nb - 1,
                                      [TweetB | ReversedResultTweets])
    end.


%% @doc Returns the Nb more recent tweets from the given list of the list of tweets,
%% sorted in decreasing order.
%% Warning! Each list in the least must be have a length &lt;= Nb.
-spec sorted_tweets_limited_merge_list([tweets_t()], non_neg_integer()) -> tweets_t().
sorted_tweets_limited_merge_list([], _Nb) ->
    [];
sorted_tweets_limited_merge_list([Tweets], Nb) ->
    ?assert(is_tweets_t(Tweets)),
    ?assert(is_integer(Nb)),
    ?assert(Nb >= 0),
    lists:sublist(Tweets, Nb);
sorted_tweets_limited_merge_list([TweetsA, TweetsB | RemainListTweets], Nb) ->
    ?assert(is_tweets_t(TweetsA)),
    ?assert(is_tweets_t(TweetsB)),
    ?assert(is_integer(Nb)),
    ?assert(Nb >= 0),
    sorted_tweets_limited_merge_list([sorted_tweets_limited_merge(TweetsA, TweetsB, Nb)
                                      | RemainListTweets],
                                     Nb).


%% @doc Merges 2 sorted list of tweets
%% and returns the result in a sorted list of all tweets.
-spec sorted_tweets_merge(tweets_t(), tweets_t()) -> tweets_t().
sorted_tweets_merge(TweetsA, TweetsB) ->
    ?assert(is_tweets_t(TweetsA)),
    ?assert(is_tweets_t(TweetsB)),
    lists:merge(fun is_ordonned_tweet/2, TweetsA, TweetsB).


%% @doc Returns a string representation of the local date/time corresponding to TimeStamp
%% Adapted from https://gist.github.com/nakamegu/104ae0bcad9ba88f89d4
-spec timestamp_to_datetime_string(tweet_time_t()) -> string().
timestamp_to_datetime_string(TimeStamp) ->
    ?assert(is_tweet_time_t(TimeStamp)),
    UnixEpochGS = calendar:datetime_to_gregorian_seconds({{1970, 1, 1}, {0, 0, 0}}),
    GregorianSeconds = UnixEpochGS + erlang:convert_time_unit(TimeStamp, native, second),
    {{Year, Day, Month}, {Hour, Minute, Second}} = calendar:universal_time_to_local_time(calendar:gregorian_seconds_to_datetime(GregorianSeconds)),
    lists:flatten(
      io_lib:format("~B/~B/~B ~Bh~Bm~Bs",
                    [Year, Day, Month, Hour, Minute, Second])).


%% @doc Returns a string representation of a tweet.
-spec tweet_to_string(tweet_t()) -> string().
tweet_to_string(Tweet) ->
    ?assert(is_tweet_t(Tweet)),
    {_tweet, UserId, Timestamp, TweetContent} = Tweet,
    lists:flatten(
      io_lib:format("=== User ~B at ~s (~B) === \"\"\"~s\"\"\"~n",
                    [UserId, timestamp_to_datetime_string(Timestamp), Timestamp, TweetContent])).


%% @doc Returns a string representation of the list of tweets.
-spec tweets_to_string(tweets_t()) -> string().
tweets_to_string(Tweets) ->
    ?assert(is_tweets_t(Tweets)),
    string:join(tweets_to_string_loop_(Tweets), "").

tweets_to_string_loop_([]) ->
    [];
tweets_to_string_loop_([Tweet | Remain]) ->
    ?assert(is_tweet_t(Tweet)),
    ?assert(is_tweets_t(Remain)),
    [tweet_to_string(Tweet)
     | tweets_to_string_loop_(Remain)].
