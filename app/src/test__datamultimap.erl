%%% @doc Unit tests for datamultimap.erl.
%%%
%%% @author Olivier Pirson [http://www.opimedia.be/]
%%% @copyright GPLv3 --- 2018 Olivier Pirson
%%% @version May 15, 2018

-module(test__datamultimap).

-include_lib("config.hrl").
-include_lib("debug.hrl").
-include_lib("test.hrl").

-include_lib("eunit/include/eunit.hrl").



%%
%% Helpers
%%

add_subscriptions(DataMultiMap, []) ->
    ?assert(datamultimap:is_datamultimap_t(DataMultiMap)),
    DataMultiMap;
add_subscriptions(DataMultiMap, [{UserId, UserIdToSubscribeTo} | RemainSubscriptions]) ->
    ?assert(datamultimap:is_datamultimap_t(DataMultiMap)),
    ?assert(common:is_user_id_t(UserId)),
    ?assert(maps:is_key(UserId, datamultimap:get_datamap(DataMultiMap))),
    ?assert(common:is_user_id_t(UserIdToSubscribeTo)),
    ?assertNotEqual(UserId, UserIdToSubscribeTo),
    add_subscriptions(datamultimap:add_subscription_to_user(DataMultiMap,
                                                            UserId, UserIdToSubscribeTo),
                      RemainSubscriptions).


add_tweets(DataMultiMap, UserId, TweetContentPrefix, Nb) ->
    ?assert(datamultimap:is_datamultimap_t(DataMultiMap)),
    ?assert(common:is_user_id_t(UserId)),
    ?assert(is_integer(Nb)),
    ?assert(Nb >= 1),
    add_tweets_loop_(DataMultiMap, UserId, TweetContentPrefix, Nb, 0).

add_tweets_loop_(DataMultiMap, _UserId, _TweetContentPrefix, 0, _I) ->
    ?assert(datamultimap:is_datamultimap_t(DataMultiMap)),
    DataMultiMap;
add_tweets_loop_(DataMultiMap, UserId, TweetContentPrefix, Nb, I) ->
    ?assert(datamultimap:is_datamultimap_t(DataMultiMap)),
    ?assert(common:is_user_id_t(UserId)),
    ?assert(maps:is_key(UserId, datamultimap:get_datamap(DataMultiMap))),
    ?assert(io_lib:char_list(TweetContentPrefix)),
    ?assert(is_integer(Nb)),
    ?assert(Nb >= 1),
    ?assert(is_integer(I)),
    ?assert(I >= 0),
    {{Tweets, LengthTweets,
      PageRefs},
     Subscriptions} = maps:get(UserId, datamultimap:get_datamap(DataMultiMap)),
    ?assertEqual(LengthTweets, length(Tweets)),
    ?assertEqual(test__datamap:pagerefs_length(LengthTweets), length(PageRefs)),
    ?assertEqual(datamap:build_pagerefs(Tweets, LengthTweets), PageRefs),
    {NewDataMultiMap, NewTweet} =
        datamultimap:add_tweet(DataMultiMap, UserId,
                               lists:flatten(
                                 io_lib:format("~s~B", [TweetContentPrefix, I]))),
    {{NewTweets, NewLengthTweets,
      NewPageRefs},
     NewSubscriptions} = maps:get(UserId, datamultimap:get_datamap(NewDataMultiMap)),
    ?assertEqual(NewTweet, hd(NewTweets)),
    ?assertEqual(NewLengthTweets, length(NewTweets)),
    ?assertEqual(test__datamap:pagerefs_length(NewLengthTweets), length(NewPageRefs)),
    ?assertEqual(datamap:build_pagerefs(NewTweets, NewLengthTweets), NewPageRefs),
    ?assertEqual(LengthTweets + 1, NewLengthTweets),
    if
        NewLengthTweets rem ?PAGE_SIZE == 0 ->
            ?assertEqual(length(PageRefs) + 1, length(NewPageRefs));
        true ->
            ?assertEqual(length(PageRefs), length(NewPageRefs))
    end,
    ?assertEqual(Subscriptions, NewSubscriptions),
    add_tweets_loop_(NewDataMultiMap, UserId, TweetContentPrefix, Nb - 1, I + 1).


add_users(DataMultiMap, 0) ->
    ?assert(datamultimap:is_datamultimap_t(DataMultiMap)),
    DataMultiMap;
add_users(DataMultiMap, Nb) ->
    ?assert(datamultimap:is_datamultimap_t(DataMultiMap)),
    ?assert(is_integer(Nb)),
    ?assert(Nb >= 1),
    {NewDataMultiMap, NewUserId} = datamultimap:add_new_user(DataMultiMap),
    ?assert(maps:is_key(NewUserId, datamultimap:get_datamap(NewDataMultiMap))),
    add_users(NewDataMultiMap, Nb - 1).


add_users_tweets(DataMultiMap, []) ->
    ?assert(datamultimap:is_datamultimap_t(DataMultiMap)),
    DataMultiMap;
add_users_tweets(DataMultiMap, [NbTweet | RemainNbTweet]) ->
    ?assert(datamultimap:is_datamultimap_t(DataMultiMap)),
    ?assert(is_integer(NbTweet)),
    ?assert(NbTweet >= 0),
    {NewUserDataMultiMap, NewUserId} = datamultimap:add_new_user(DataMultiMap),
    NewTweetsDataMultiMap =
        if
            NbTweet >= 1 ->
                add_tweets(NewUserDataMultiMap, NewUserId,
                           lists:flatten(
                             io_lib:format("Tweet from user ~B: ", [NewUserId])),
                           NbTweet);
            true ->
                NewUserDataMultiMap
        end,
    add_users_tweets(NewTweetsDataMultiMap, RemainNbTweet).



%%
%% Tests
%%

starting_msg_info_test() ->
    server_multi_map:initialize(),  % required to start user_id_actor_name
    ct:print("START tests in ~s with ~B processes~n", [?FILE, ?NB_DATA_ACTOR]).



add_new_user__test() ->
    user_id_actor_name ! restart,
    DataMultiMap = datamultimap:create(),

    {New0DataMultiMap, New0UserId} = datamultimap:add_new_user(DataMultiMap),
    ?assertEqual(0, New0UserId),
    ?assertEqual(#{0 => datamap:empty_value()}, datamultimap:get_datamap(New0DataMultiMap)),

    {New1DataMultiMap, New1UserId} = datamultimap:add_new_user(New0DataMultiMap),
    ?assertEqual(1, New1UserId),
    ?assertEqual(#{0 => datamap:empty_value(),
                   1 => datamap:empty_value()}, datamultimap:get_datamap(New1DataMultiMap)),

    {New2DataMultiMap, New2UserId} = datamultimap:add_new_user(New1DataMultiMap),
    ?assertEqual(2, New2UserId),
    ?assertEqual(#{0 => datamap:empty_value(),
                   1 => datamap:empty_value(),
                   2 => datamap:empty_value()}, datamultimap:get_datamap(New2DataMultiMap)).


add_subscription_to_user__test() ->
    user_id_actor_name ! restart,
    DataMultiMap = add_users(datamultimap:create(), 3),
    ?assertEqual(DataMultiMap, datamultimap:add_subscription_to_user(DataMultiMap, 1, 1)),

    New0DataMultiMap = datamultimap:add_subscription_to_user(DataMultiMap, 1, 2),
    ?assertEqual(#{0 => datamap:empty_value(),
                   1 => {{[], 0,
                          []},
                         [2]},
                   2 => datamap:empty_value()}, datamultimap:get_datamap(New0DataMultiMap)),
    ?assertEqual(New0DataMultiMap, datamultimap:add_subscription_to_user(New0DataMultiMap, 1, 2)),
    ?assertEqual(New0DataMultiMap, datamultimap:add_subscription_to_user(New0DataMultiMap, 1, 1)),

    New1DataMultiMap = datamultimap:add_subscription_to_user(New0DataMultiMap, 1, 0),
    ?assertEqual(#{0 => datamap:empty_value(),
                   1 => {{[], 0,
                          []},
                         [0, 2]},
                   2 => datamap:empty_value()}, datamultimap:get_datamap(New1DataMultiMap)),

    New2DataMultiMap = datamultimap:add_subscription_to_user(New1DataMultiMap, 2, 1),
    ?assertEqual(#{0 => datamap:empty_value(),
                   1 => {{[], 0,
                          []},
                         [0, 2]},
                   2 => {{[], 0,
                          []},
                         [1]}}, datamultimap:get_datamap(New2DataMultiMap)),
    ?assertEqual(New2DataMultiMap, datamultimap:add_subscription_to_user(New2DataMultiMap, 0, 0)),

    %% Follows invalid user.
    New3DataMultiMap = datamultimap:add_subscription_to_user(New2DataMultiMap, 1, 1001),
    ?assertEqual(#{0 => datamap:empty_value(),
                   1 => {{[], 0,
                          []},
                         [1001, 0, 2]},
                   2 => {{[], 0,
                          []},
                         [1]}}, datamultimap:get_datamap(New3DataMultiMap)).


add_tweet__test() ->
    user_id_actor_name ! restart,
    DataMultiMap = add_users(datamultimap:create(), 3),
    Timestamp = common:current_time(),

    {New0DataMultiMap, New0Tweet} = datamultimap:add_tweet(DataMultiMap, 1, "A"),
    {tweet, 1, New0Timestamp, "A"} = New0Tweet,
    ?assert(New0Timestamp > Timestamp),
    ?assertEqual(#{0 => datamap:empty_value(),
                   1 => {{[New0Tweet], 1,
                          []},
                         []},
                   2 => datamap:empty_value()}, datamultimap:get_datamap(New0DataMultiMap)),

    {New1DataMultiMap, New1Tweet} = datamultimap:add_tweet(New0DataMultiMap, 0, "B"),
    {tweet, 0, New1Timestamp, "B"} = New1Tweet,
    ?assert(New1Timestamp > New0Timestamp),
    ?assertEqual(#{0 => {{[New1Tweet], 1,
                          []},
                         []},
                   1 => {{[New0Tweet], 1,
                          []},
                         []},
                   2 => datamap:empty_value()}, datamultimap:get_datamap(New1DataMultiMap)),

    {New2DataMultiMap, New2Tweet} = datamultimap:add_tweet(New1DataMultiMap, 1, "C"),
    {tweet, 1, New2Timestamp, "C"} = New2Tweet,
    ?assert(New2Timestamp > New1Timestamp),
    ?assertEqual(#{0 => {{[New1Tweet], 1,
                          []},
                         []},
                   1 => {{[New2Tweet, New0Tweet], 2,
                          []},
                         []},
                   2 => datamap:empty_value()}, datamultimap:get_datamap(New2DataMultiMap)),

    New3DataMultiMap = add_tweets(New2DataMultiMap, 0, "Content", ?PAGE_SIZE*10),
    {{New3Tweets, ?PAGE_SIZE*10 + 1,
      PageRefs},
     []} = maps:get(0, datamultimap:get_datamap(New3DataMultiMap)),
    ?assertEqual(?PAGE_SIZE*10 + 1, length(New3Tweets)),
    ?assertEqual(10, length(PageRefs)).


add_tweet__page__test() ->
    user_id_actor_name ! restart,
    DataMultiMap = add_tweets(add_users(datamultimap:create(), 3),
                              1, "Tweet of user 1 ", ?PAGE_SIZE - 1),

    DataMap = datamultimap:get_datamap(DataMultiMap),
    ?assertEqual(maps:get(0, DataMap), datamap:empty_value()),
    ?assertEqual(maps:get(2, DataMap), datamap:empty_value()),
    {{Tweets, ?PAGE_SIZE - 1,
      []},
     []} = maps:get(1, DataMap),
    ?assertEqual(?PAGE_SIZE - 1, length(Tweets)),

    {New0DataMultiMap, New0Tweet} = datamultimap:add_tweet(DataMultiMap, 1, "Added 1"),
    New0DataMap = datamultimap:get_datamap(New0DataMultiMap),
    ?assertEqual(maps:get(0, New0DataMap), datamap:empty_value()),
    ?assertEqual(maps:get(2, New0DataMap), datamap:empty_value()),
    {{New0Tweets, ?PAGE_SIZE,
      [New0Tweets]},
     []} = maps:get(1, New0DataMap),
    ?assertEqual(?PAGE_SIZE, length(New0Tweets)),
    ?assertEqual([New0Tweet | Tweets], New0Tweets),

    {New1DataMultiMap, New1Tweet} = datamultimap:add_tweet(New0DataMultiMap, 1, "Added 2"),
    New1DataMap = datamultimap:get_datamap(New1DataMultiMap),
    ?assertEqual(maps:get(0, New1DataMap), datamap:empty_value()),
    ?assertEqual(maps:get(2, New1DataMap), datamap:empty_value()),
    {{New1Tweets, ?PAGE_SIZE + 1,
      [New0Tweets]},
     []} = maps:get(1, New1DataMap),
    ?assertEqual(?PAGE_SIZE + 1, length(New1Tweets)),
    ?assertEqual([New1Tweet | New0Tweets], New1Tweets),

    New2DataMultiMap = add_tweets(New1DataMultiMap,
                                  1, "Tweet of user 1, second step ", ?PAGE_SIZE - 2),
    New2DataMap = datamultimap:get_datamap(New2DataMultiMap),
    ?assertEqual(maps:get(0, New2DataMap), datamap:empty_value()),
    ?assertEqual(maps:get(2, New2DataMap), datamap:empty_value()),
    {{New2Tweets, ?PAGE_SIZE*2 - 1,
      [New0Tweets]},
     []} = maps:get(1, New2DataMap),
    ?assertEqual(?PAGE_SIZE*2 - 1, length(New2Tweets)),

    {New3DataMultiMap, New3Tweet} = datamultimap:add_tweet(New2DataMultiMap, 1, "Added 3"),
    New3DataMap = datamultimap:get_datamap(New3DataMultiMap),
    ?assertEqual(maps:get(0, New3DataMap), datamap:empty_value()),
    ?assertEqual(maps:get(2, New3DataMap), datamap:empty_value()),
    {{New3Tweets, ?PAGE_SIZE*2,
      [New3Tweets, New0Tweets]},
     []} = maps:get(1, New3DataMap),
    ?assertEqual(?PAGE_SIZE*2, length(New3Tweets)),
    ?assertEqual([New3Tweet | New2Tweets], New3Tweets),

    {New4DataMultiMap, New4Tweet} = datamultimap:add_tweet(New3DataMultiMap, 1, "Added 4"),
    New4DataMap = datamultimap:get_datamap(New4DataMultiMap),
    ?assertEqual(maps:get(0, New4DataMap), datamap:empty_value()),
    ?assertEqual(maps:get(2, New4DataMap), datamap:empty_value()),
    {{New4Tweets, ?PAGE_SIZE*2 + 1,
      [New3Tweets, New0Tweets]},
     []} = maps:get(1, New4DataMap),
    ?assertEqual(?PAGE_SIZE*2 + 1, length(New4Tweets)),
    ?assertEqual([New4Tweet | New3Tweets], New4Tweets).

append_recent_tweets_map__test() ->
    user_id_actor_name ! restart,
    DataMultiMap = datamultimap:create(),
    ?assertEqual(datamultimap:create(), DataMultiMap),
    {OtherServerPids, _DataMap, RecentTweets} = DataMultiMap,
    ?assertEqual([], OtherServerPids),
    ?assertEqual(RecentTweets, maps:new()),

    ?assertEqual(datamultimap:create(),
                 datamultimap:append_recent_tweets_map(DataMultiMap, maps:new())),

    user_id_actor_name ! restart,
    Recent0TweetsMap = maps:map(fun(_UserId,
                                    {{Tweets, LengthTweets,
                                      _PageRefs},
                                     _Subscriptions}) ->
                                        ?assertEqual(LengthTweets, length(Tweets)),
                                        Tweets
                                end,
                                datamultimap:get_datamap(
                                  add_users_tweets(datamultimap:create(), [2, 0, 3, 42, 5]))),


    New0DataMultiMap = datamultimap:append_recent_tweets_map(DataMultiMap, Recent0TweetsMap),

    {New0OtherServerPids, New0DataMap, New0RecentTweets} = New0DataMultiMap,
    ?assertEqual([], New0OtherServerPids),
    ?assertEqual(New0RecentTweets, maps:new()),
    ?assertEqual(lists:seq(0, 4), lists:sort(maps:keys(New0DataMap))),
    maps:map(fun(UserId, {{Tweets, LengthTweets,
                           PageRefs},
                          Subscriptions}) ->
                     ?assertEqual(LengthTweets, length(Tweets)),
                     ?assertEqual(datamap:build_pagerefs(Tweets, LengthTweets), PageRefs),
                     ?assertEqual([], Subscriptions),
                     ?assertEqual(maps:get(UserId, Recent0TweetsMap), Tweets)
             end,
             New0DataMap),

    ?assertEqual(New0DataMultiMap,
                 datamultimap:append_recent_tweets_map(New0DataMultiMap, maps:new())),

    user_id_actor_name ! restart,
    Recent1TweetsMap = maps:map(fun(_UserId,
                                    {{Tweets, LengthTweets,
                                      _PageRefs},
                                     _Subscriptions}) ->
                                        ?assertEqual(LengthTweets, length(Tweets)),
                                        Tweets
                                end,
                                datamultimap:get_datamap(
                                  add_users_tweets(datamultimap:create(), [3, 6, 0, 7]))),

    user_id_actor_name ! restart,
    Recent2TweetsMap = maps:map(fun(_UserId,
                                    {{Tweets, LengthTweets,
                                      _PageRefs},
                                     _Subscriptions}) ->
                                        ?assertEqual(LengthTweets, length(Tweets)),
                                        Tweets
                                end,
                                datamultimap:get_datamap(
                                  add_users_tweets(datamultimap:create(), [1, 2, 0, 4, 5, 6]))),


    New1DataMultiMap = datamultimap:append_recent_tweets_map(New0DataMultiMap, Recent1TweetsMap),

    {New1OtherServerPids, New1DataMap, New1RecentTweets} = New1DataMultiMap,
    ?assertEqual([], New1OtherServerPids),
    ?assertEqual(New1RecentTweets, maps:new()),
    ?assertEqual(lists:seq(0, 4), lists:sort(maps:keys(New1DataMap))),
    maps:map(fun(UserId, {{Tweets, LengthTweets,
                           PageRefs},
                          Subscriptions}) ->
                     ?assertEqual(LengthTweets, length(Tweets)),
                     ?assertEqual(datamap:build_pagerefs(Tweets, LengthTweets), PageRefs),
                     ?assertEqual([], Subscriptions),

                     {{New0Tweets, New0LengthTweets,
                       New0PageRefs},
                      New0Subscriptions} = maps:get(UserId, New0DataMap),
                     ?assertEqual(New0LengthTweets, length(New0Tweets)),
                     ?assertEqual(datamap:build_pagerefs(New0Tweets, New0LengthTweets),
                                  New0PageRefs),
                     ?assertEqual([], New0Subscriptions),

                     AddedTweets = maps:get(UserId, Recent1TweetsMap, []),
                     CorrectTweets = common:sorted_tweets_merge(New0Tweets, AddedTweets),
                     ?assertEqual(length(CorrectTweets), New0LengthTweets + length(AddedTweets)),
                     ?assertEqual(length(CorrectTweets), length(Tweets)),
                     ?assertEqual(CorrectTweets, Tweets)
             end,
             New1DataMap),

    New2DataMultiMap = datamultimap:append_recent_tweets_map(New1DataMultiMap, Recent2TweetsMap),

    {New2OtherServerPids, New2DataMap, New2RecentTweets} = New2DataMultiMap,
    ?assertEqual([], New2OtherServerPids),
    ?assertEqual(New2RecentTweets, maps:new()),
    ?assertEqual(lists:seq(0, 5), lists:sort(maps:keys(New2DataMap))),
    maps:map(fun(UserId, {{Tweets, LengthTweets,
                           PageRefs},
                          Subscriptions}) ->
                     ?assertEqual(LengthTweets, length(Tweets)),
                     ?assertEqual(datamap:build_pagerefs(Tweets, LengthTweets), PageRefs),
                     ?assertEqual([], Subscriptions),

                     IsKey = maps:is_key(UserId, New1DataMap),
                     if
                         IsKey ->
                             {{New1Tweets, New1LengthTweets,
                               New1PageRefs},
                              New1Subscriptions} = maps:get(UserId, New1DataMap),
                             ?assertEqual(New1LengthTweets, length(New1Tweets)),
                             ?assertEqual(datamap:build_pagerefs(New1Tweets, New1LengthTweets),
                                          New1PageRefs),
                             ?assertEqual([], New1Subscriptions);
                         true ->
                             ?assertEqual(5, UserId),
                             New1Tweets = [],
                             New1LengthTweets = 0
                     end,

                     AddedTweets = maps:get(UserId, Recent2TweetsMap, []),
                     CorrectTweets = common:sorted_tweets_merge(New1Tweets, AddedTweets),
                     ?assertEqual(length(CorrectTweets), New1LengthTweets + length(AddedTweets)),
                     ?assertEqual(length(CorrectTweets), length(Tweets)),
                     ?assertEqual(CorrectTweets, Tweets)
             end,
             New2DataMap).


create__test() ->
    ?assertEqual({[], datamap:create(), datamultimap:empty_recent_tweets()},
                 datamultimap:create()).


empty_recent_tweets__test() ->
    ?assertEqual(#{}, datamultimap:empty_recent_tweets()).


get_datamap__test() ->
    user_id_actor_name ! restart,
    DataMultiMap = datamultimap:create(),
    {_OtherServerPids, DataMap, _RecentTweetsMap} = DataMultiMap,
    ?assertEqual(DataMap, datamultimap:get_datamap(DataMultiMap)),

    NewDataMultiMap = add_users_tweets(DataMultiMap, lists:seq(0, 20)),
    {_NewOtherServerPids, NewDataMap, _NewRecentTweetsMap} = NewDataMultiMap,
    ?assertEqual(NewDataMap, datamultimap:get_datamap(NewDataMultiMap)).


get_other_server_pids__test() ->
    user_id_actor_name ! restart,
    DataMultiMap = datamultimap:set_other_server_pids(datamultimap:create(), [], self()),
    ?assertEqual([], datamultimap:get_other_server_pids(DataMultiMap)),

    New0DataMultiMap = datamultimap:set_other_server_pids(DataMultiMap, [self()], self()),
    ?assertEqual([], datamultimap:get_other_server_pids(DataMultiMap)),

    New1DataMultiMap = datamultimap:set_other_server_pids(New0DataMultiMap, [self()], self()),
    ?assertEqual([], datamultimap:get_other_server_pids(DataMultiMap)),

    Pids = lists:map(fun(_) ->
                             spawn_link(fun() ->
                                                exit(normal)
                                        end)
                     end,
                     lists:seq(1, 13)),
    New2DataMultiMap = datamultimap:set_other_server_pids(New1DataMultiMap,
                                                          [self() | Pids],
                                                          self()),
    ?assertEqual(Pids, datamultimap:get_other_server_pids(New2DataMultiMap)),

    New3DataMultiMap = datamultimap:set_other_server_pids(New2DataMultiMap, Pids, hd(Pids)),
    ?assertEqual(tl(Pids), datamultimap:get_other_server_pids(New3DataMultiMap)),

    New4DataMultiMap = datamultimap:set_other_server_pids(New3DataMultiMap, Pids, self()),
    ?assertEqual(Pids, datamultimap:get_other_server_pids(New4DataMultiMap)),

    New5DataMultiMap = datamultimap:set_other_server_pids(New4DataMultiMap, Pids,
                                                          lists:nth(7, Pids)),
    ?assertEqual(lists:append(lists:sublist(Pids, 6), lists:nthtail(7, Pids)),
                 datamultimap:get_other_server_pids(New5DataMultiMap)).


get_timeline__without_following__test() ->
    {timeout, 60,
     fun() ->
             user_id_actor_name ! restart,
             Nbs = [7, 0, 5, 1, 2, 3, 4,
                    ?PAGE_SIZE   - 1, ?PAGE_SIZE,   ?PAGE_SIZE   + 1,
                    ?PAGE_SIZE*2 - 1, ?PAGE_SIZE*2, ?PAGE_SIZE*2 + 1,
                    ?PAGE_SIZE*3 - 1, ?PAGE_SIZE*3, ?PAGE_SIZE*3 + 1,
                    ?PAGE_SIZE*4 - 1, ?PAGE_SIZE*4, ?PAGE_SIZE*4 + 1,
                    ?PAGE_SIZE*5 - 1, ?PAGE_SIZE*5, ?PAGE_SIZE*5 + 1,
                    ?PAGE_SIZE*6 - 1, ?PAGE_SIZE*6, ?PAGE_SIZE*6 + 1],
             DataMultiMap = add_users_tweets(datamultimap:create(), Nbs),
             DataMap = datamultimap:get_datamap(DataMultiMap),
             ?assertEqual(lists:seq(0, length(Nbs) - 1), lists:sort(maps:keys(DataMap))),

             lists:foreach(
               fun(I) ->
                       Nb = lists:nth(I, Nbs),
                       {{AllTweets, LengthAllTweets,
                         _PageRefs},
                        _Subscriptions} = maps:get(I - 1, DataMap),
                       ?assertEqual(length(AllTweets), LengthAllTweets),
                       ?assertEqual(Nb, LengthAllTweets),

                       lists:foreach(
                         fun(Page) ->
                                 Tweets = datamultimap:get_timeline(DataMultiMap, I - 1, Page),
                                 PageNb = min(?PAGE_SIZE,
                                              max(0,
                                                  Nb - ?PAGE_SIZE * (Page - 1))),
                                 ?assertEqual(PageNb, length(Tweets)),
                                 ?assertEqual(lists:sort(fun common:is_ordonned_tweet/2, Tweets),
                                              Tweets),
                                 ?assertEqual(datamap:get_timeline(DataMap, I - 1, Page),
                                              Tweets),

                                 CorrectTweets = try
                                                     lists:sublist(AllTweets,
                                                                   ?PAGE_SIZE * (Page - 1) + 1,
                                                                   ?PAGE_SIZE)
                                                 catch
                                                     _Exception:_Reason -> []
                                                 end,
                                 ?assertEqual(CorrectTweets, Tweets),

                                 ?assertEqual(datamultimap:get_tweets(DataMultiMap, I - 1, Page),
                                              Tweets)
                         end,
                         lists:seq(1, 10))
               end,
               lists:seq(1, length(Nbs)))
     end}.


get_timeline__with_following__test() ->
    {timeout, 60,
     fun() ->
             user_id_actor_name ! restart,
             Nbs = [7, 0, 5, 1, 2, 3, 4,
                    ?PAGE_SIZE   - 1, ?PAGE_SIZE,   ?PAGE_SIZE   + 1,
                    ?PAGE_SIZE*2 - 1, ?PAGE_SIZE*2, ?PAGE_SIZE*2 + 1,
                    ?PAGE_SIZE*3 - 1, ?PAGE_SIZE*3, ?PAGE_SIZE*3 + 1,
                    ?PAGE_SIZE*4 - 1, ?PAGE_SIZE*4, ?PAGE_SIZE*4 + 1,
                    ?PAGE_SIZE*5 - 1, ?PAGE_SIZE*5, ?PAGE_SIZE*5 + 1,
                    ?PAGE_SIZE*6 - 1, ?PAGE_SIZE*6, ?PAGE_SIZE*6 + 1],
             DataMultiMap = add_subscriptions(add_users_tweets(datamultimap:create(), Nbs),
                                              [{0, 0},
                                               {1, 10},
                                               {2, 3}, {2, 1001},
                                               {3, 2}, {3, 3}, {3, 4}, {3, 1002},
                                               {15, 17}, {15, 19}, {15, 14},
                                               {16, 17}, {16, 19}, {16, 14}]),
             DataMap = datamultimap:get_datamap(DataMultiMap),
             ?assertEqual(lists:seq(0, length(Nbs) - 1), lists:sort(maps:keys(DataMap))),

             lists:foreach(
               fun(I) ->
                       Nb = lists:nth(I, Nbs),
                       AllTweets = test__datamap:get_timeline_all(DataMap, I - 1),
                       LengthAllTweets = length(AllTweets),
                       ?assert(Nb =< LengthAllTweets),

                       lists:foreach(
                         fun(Page) ->
                                 Tweets = datamultimap:get_timeline(DataMultiMap, I - 1, Page),
                                 PageNb = min(?PAGE_SIZE,
                                              max(0,
                                                  LengthAllTweets - ?PAGE_SIZE * (Page - 1))),
                                 ?assert(PageNb == length(Tweets)),
                                 ?assertEqual(lists:sort(fun common:is_ordonned_tweet/2, Tweets),
                                              Tweets),

                                 CorrectTweets
                                     = try
                                           lists:sublist(AllTweets, ?PAGE_SIZE * (Page - 1) + 1,
                                                         ?PAGE_SIZE)
                                       catch
                                           _Exception:_Reason -> []
                                       end,
                                 ?assertEqual(CorrectTweets, Tweets),

                                 Subscriptions = datamap:get_subscriptions(DataMap, I - 1),
                                 case Subscriptions of
                                     [] -> ?assertEqual(datamap:get_tweets(DataMap, I - 1, Page),
                                                        Tweets);
                                     _ -> ok
                                 end
                         end,
                         lists:seq(1, 10))
               end,
               lists:seq(1, length(Nbs)))
     end}.


get_tweets__test() ->
    user_id_actor_name ! restart,
    Nbs = [7, 0, 5, 1, 2, 3, 4,
           ?PAGE_SIZE   - 1, ?PAGE_SIZE,   ?PAGE_SIZE   + 1,
           ?PAGE_SIZE*2 - 1, ?PAGE_SIZE*2, ?PAGE_SIZE*2 + 1,
           ?PAGE_SIZE*3 - 1, ?PAGE_SIZE*3, ?PAGE_SIZE*3 + 1,
           ?PAGE_SIZE*4 - 1, ?PAGE_SIZE*4, ?PAGE_SIZE*4 + 1,
           ?PAGE_SIZE*5 - 1, ?PAGE_SIZE*5, ?PAGE_SIZE*5 + 1,
           ?PAGE_SIZE*6 - 1, ?PAGE_SIZE*6, ?PAGE_SIZE*6 + 1],
    DataMultiMap = add_users_tweets(datamultimap:create(), Nbs),
    DataMap = datamultimap:get_datamap(DataMultiMap),
    ?assertEqual(lists:seq(0, length(Nbs) - 1), lists:sort(maps:keys(DataMap))),

    lists:foreach(
      fun(I) ->
              Nb = lists:nth(I, Nbs),
              {{AllTweets, LengthAllTweets,
                _PageRefs},
               _Subscriptions} = maps:get(I - 1, DataMap),
              ?assertEqual(length(AllTweets), LengthAllTweets),
              ?assertEqual(Nb, LengthAllTweets),

              lists:foreach(
                fun(Page) ->
                        Tweets = datamultimap:get_tweets(DataMultiMap, I - 1, Page),
                        PageNb = min(?PAGE_SIZE,
                                     max(0,
                                         Nb - ?PAGE_SIZE * (Page - 1))),
                        ?assertEqual(PageNb, length(Tweets)),
                        ?assertEqual(lists:sort(fun common:is_ordonned_tweet/2, Tweets), Tweets),
                        ?assertEqual(datamap:get_tweets(DataMap, I - 1, Page),
                                     Tweets),

                        CorrectTweets = try
                                            lists:sublist(AllTweets, ?PAGE_SIZE * (Page - 1) + 1,
                                                          ?PAGE_SIZE)
                                        catch
                                            _Exception:_Reason -> []
                                        end,
                        ?assertEqual(CorrectTweets, Tweets)
                end,
                lists:seq(1, 10))
      end,
      lists:seq(1, length(Nbs))).


is_datamultimap_t__test() ->
    ?assert(datamultimap:is_datamultimap_t(datamultimap:create())),
    ?assert(datamultimap:is_datamultimap_t(add_users_tweets(datamultimap:create(),
                                                            lists:seq(0, 13)))),

    ?assertNot(datamultimap:is_datamultimap_t(maps:new())),
    ?assertNot(datamultimap:is_datamultimap_t([])),
    ?assertNot(datamultimap:is_datamultimap_t([42])),
    ?assertNot(datamultimap:is_datamultimap_t(42)).


is_recent_tweets_map_t__test() ->
    ?assert(datamultimap:is_recent_tweets_map_t(datamultimap:empty_recent_tweets())),
    ?assert(datamultimap:is_recent_tweets_map_t(maps:new())),

    {TweetA, TweetB} = test:two_tweet(42, 666, "TweetA", "TweetB"),
    ?assert(datamultimap:is_recent_tweets_map_t(#{42 => [TweetA]})),
    ?assert(datamultimap:is_recent_tweets_map_t(#{666 => [TweetB]})),
    ?assert(datamultimap:is_recent_tweets_map_t(#{42 => [TweetA],
                                                  666 => [TweetB]})),

    {TweetC, TweetD} = test:two_tweet(42, 36, "TweetC", "TweetD"),
    ?assert(datamultimap:is_recent_tweets_map_t(#{42 => [TweetA, TweetC],
                                                  666 => [TweetB],
                                                  36 => [TweetD]})),

    ?assertNot(datamultimap:is_recent_tweets_map_t([])),
    ?assertNot(datamultimap:is_recent_tweets_map_t([42])),
    ?assertNot(datamultimap:is_recent_tweets_map_t(42)),
    ?assertNot(datamultimap:is_recent_tweets_map_t(#{42 => TweetA})),
    ?assertNot(datamultimap:is_recent_tweets_map_t(#{666 => [TweetA]})),
    ?assertNot(datamultimap:is_recent_tweets_map_t(#{42 => [TweetA],
                                                     123 => [TweetB]})).


set_other_server_pids__test() ->
    user_id_actor_name ! restart,
    DataMultiMap = datamultimap:set_other_server_pids(datamultimap:create(), [], self()),
    ?assertEqual(datamultimap:create(), DataMultiMap),

    New0DataMultiMap = datamultimap:set_other_server_pids(DataMultiMap, [self()], self()),
    ?assertEqual(datamultimap:create(), New0DataMultiMap),

    New1DataMultiMap = datamultimap:set_other_server_pids(New0DataMultiMap, [self()], self()),
    ?assertEqual(datamultimap:create(), New1DataMultiMap),

    Pids = lists:map(fun(_) ->
                             spawn_link(fun() ->
                                                exit(normal)
                                        end)
                     end,
                     lists:seq(1, 13)),
    New2DataMultiMap = datamultimap:set_other_server_pids(New1DataMultiMap,
                                                          [self() | Pids],
                                                          self()),
    ?assertEqual(Pids, datamultimap:get_other_server_pids(New2DataMultiMap)),

    New3DataMultiMap = datamultimap:set_other_server_pids(New2DataMultiMap, Pids, hd(Pids)),
    ?assertEqual(tl(Pids), datamultimap:get_other_server_pids(New3DataMultiMap)),

    New4DataMultiMap = datamultimap:set_other_server_pids(New3DataMultiMap, Pids, self()),
    ?assertEqual(Pids, datamultimap:get_other_server_pids(New4DataMultiMap)),

    New5DataMultiMap = datamultimap:set_other_server_pids(New4DataMultiMap, Pids,
                                                          lists:nth(7, Pids)),
    ?assertEqual(lists:append(lists:sublist(Pids, 6), lists:nthtail(7, Pids)),
                 datamultimap:get_other_server_pids(New5DataMultiMap)).
