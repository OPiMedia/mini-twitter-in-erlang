%%% @doc Data structure to save all users and tweets,
%%% and associated functions.
%%%
%%% @author Olivier Pirson [http://www.opimedia.be/]
%%% @copyright GPLv3 --- 2018 Olivier Pirson
%%% @version May 15, 2018

-module(datamap).

-include_lib("stdlib/include/assert.hrl").

-include("config.hrl").
-include("debug.hrl").

-export([add_new_user/1,
         add_subscription_to_user/3,
         add_tweet/3,
         add_user/2,
         build_pagerefs/2,
         create/0,
         empty_value/0,
         get_subscriptions/2,
         get_timeline/3,
         get_tweets/3,
         is_datamap_t/1]).

-export_type([datamap_t/0]).



%% @type datamap_t(). Data structure for all data (users and tweets). Map:
%%   user id => {{list of tweets, length of this list,
%%                list of list of tweets (used to be access fastly to the right page)},
%%               list of followed users}
-type datamap_t() :: #{common:user_id_t() => {{common:tweets_t(), non_neg_integer(),
                                               [common:tweets_t()]},
                                              [common:user_id_t()]}}.



%% @doc Returns a new structure with a new user added and the id of this new user.
-spec add_new_user(datamap_t()) -> {datamap_t(), common:user_id_t()}.
add_new_user(DataMap) ->
    ?assert(is_datamap_t(DataMap)),
    NewUserId = maps:size(DataMap),
    {add_user(DataMap, NewUserId), NewUserId}.


%% @doc Returns a new structure with the user UserIdToSubscribeTo followed by UserId.
%% If UserId == UserIdToSubscribeTo then returns Data.
%% If UserIdToSubscribeTo was already followed by UserId, then returns DataMap.
%% UserId and UserIdToSubscribeTo must be valid user ids.
%% UserIdToSubscribeTo may be invalid but must be >= 0.
-spec add_subscription_to_user(datamap_t(), common:user_id_t(), common:user_id_t()) -> datamap_t().
add_subscription_to_user(DataMap, UserId, UserIdToSubscribeTo) ->
    ?assert(is_datamap_t(DataMap)),
    ?assert(common:is_user_id_t(UserId)),
    ?assert(maps:is_key(UserId, DataMap)),
    ?assert(common:is_user_id_t(UserIdToSubscribeTo)),
    if
        UserIdToSubscribeTo /= UserId ->
            {DataTweets, Subscriptions} = maps:get(UserId, DataMap),
            Already = lists:member(UserIdToSubscribeTo, Subscriptions),
            if
                Already -> DataMap;
                true ->
                    maps:update(UserId, {DataTweets,
                                         [UserIdToSubscribeTo | Subscriptions]},
                                DataMap)
            end;
        true ->
            DataMap
    end.


%% @doc Returns a new structure with the tweet, and the tweet.
%% UserId must be valid user id.
-spec add_tweet(datamap_t(), common:user_id_t(), common:tweet_content_t()) -> {datamap_t(), common:tweet_t()}.
add_tweet(DataMap, UserId, TweetContent) ->
    ?assert(is_datamap_t(DataMap)),
    ?assert(common:is_user_id_t(UserId)),
    ?assert(maps:is_key(UserId, DataMap)),
    ?assert(common:is_tweet_content_t(TweetContent)),
    Timestamp = common:current_time(),
    {{Tweets, LengthTweets,
      PageRefs},
     Subscriptions} = maps:get(UserId, DataMap),
    ?assert(length(Tweets) == LengthTweets),
    NewTweet = {tweet, UserId, Timestamp, TweetContent},
    NewTweets = [NewTweet | Tweets],
    NewLengthTweets = LengthTweets + 1,
    NewPageRefs = if
                      NewLengthTweets rem ?PAGE_SIZE == 0 ->  % the new tweet completes a new page
                          [NewTweets | PageRefs];
                      true ->
                          PageRefs
                  end,
    {maps:put(UserId, {{NewTweets, NewLengthTweets,
                        NewPageRefs},
                       Subscriptions},
              DataMap),
     NewTweet}.


%% @doc Returns a new structure with the new user added.
-spec add_user(datamap_t(), common:user_id_t()) -> datamap_t().
add_user(DataMap, UserId) ->
    ?assert(is_datamap_t(DataMap)),
    ?assert(common:is_user_id_t(UserId)),
    ?assert(not maps:is_key(UserId, DataMap)),
    maps:put(UserId, empty_value(), DataMap).


%% @doc Returns the corresponding pageref to the list of tweets.
%% LengthTweets must be the length of Tweets.
-spec build_pagerefs(common:tweets_t(), non_neg_integer()) -> [common:tweets_t()].
build_pagerefs(Tweets, LengthTweets) ->
    ?assert(common:is_tweets_t(Tweets)),
    ?assert(is_integer(LengthTweets)),
    ?assert(LengthTweets >= 0),
    ?assert(LengthTweets == length(Tweets)),
    build_pagerefs_loop_(lists:nthtail(LengthTweets rem ?PAGE_SIZE, Tweets)).

-spec build_pagerefs_loop_(common:tweets_t()) -> [common:tweets_t()].
build_pagerefs_loop_([]) ->
    [];
build_pagerefs_loop_(Tweets) ->
    ?assert(common:is_tweets_t(Tweets)),
    [Tweets | build_pagerefs_loop_(lists:nthtail(?PAGE_SIZE, Tweets))].


%% @doc Returns a empty data structure datamap_t.
-spec create() -> datamap_t().
create() ->
    maps:new().


%% @doc Empty value associated with each new user in the datamap_t() data structure.
-spec empty_value() -> {{[], 0,
                          []},
                         []}.
empty_value() ->
    {{[], 0,
      []},
     []}.


%% @doc Returns the list of followed users by the given user.
%% UserId must be valid user id.
-spec get_subscriptions(datamap_t(), common:user_id_t()) -> [common:user_id_t()].
get_subscriptions(DataMap, UserId) ->
    ?assert(is_datamap_t(DataMap)),
    ?assert(common:is_user_id_t(UserId)),
    ?assert(maps:is_key(UserId, DataMap)),
    {{_Tweets, _LengthTweets,
      _PageRefs},
     Subscriptions} = maps:get(UserId, DataMap),
    Subscriptions.


-define(GET_TIMELINE_FOLDL, 1).

%% @doc Returns the given page of the timeline of the given user.
%% UserId must be valid user id.
%% Unknown followed users are ignored.
-spec get_timeline(datamap_t(), common:user_id_t(), common:page_t()) -> common:tweets_t().
-ifdef(GET_TIMELINE_FOLDL).
get_timeline(DataMap, UserId, Page) ->
    ?assert(is_datamap_t(DataMap)),
    ?assert(common:is_user_id_t(UserId)),
    ?assert(maps:is_key(UserId, DataMap)),
    ?assert(common:is_page_t(Page)),
    {{Tweets, _LengthTweets,
      _PageRefs},
     Subscriptions} = maps:get(UserId, DataMap),
    case Subscriptions of
        [] -> get_tweets(DataMap, UserId, Page);
        _ ->
            EnoughNb = common:page_to_enough_nb(Page),
            common:list_page(
              lists:foldl(fun(OtherUserId, AllTweets) ->
                                  ?assert(OtherUserId >= 0),
                                  {{OtherTweets, _OtherLengthTweets,
                                    _OtherPageRefs},
                                   _OtherSubscriptions} = maps:get(OtherUserId, DataMap,
                                                                   empty_value()),
                                  common:sorted_tweets_limited_merge(AllTweets, OtherTweets, EnoughNb)
                          end,
                          Tweets,
                          Subscriptions),
              Page)
    end.
-else.
get_timeline(DataMap, UserId, Page) ->
    ?assert(is_datamap_t(DataMap)),
    ?assert(common:is_user_id_t(UserId)),
    ?assert(maps:is_key(UserId, DataMap)),
    ?assert(common:is_page_t(Page)),
    EnoughNb = common:page_to_enough_nb(Page),
    {{Tweets, _LengthTweets,
      _PageRefs},
     Subscriptions} = maps:get(UserId, DataMap),
    ListEnoughTweets =
        [Tweets
         | lists:map(fun(OtherUserId) ->
                             {{OtherTweets, _OtherLengthTweets,
                               _OtherPageRefs},
                              _OtherSubscriptions} = maps:get(OtherUserId, DataMap),
                             ?assert(maps:is_key(OtherUserId, DataMap)),
                             OtherTweets
                     end,
                     Subscriptions)],
    common:list_page(common:sorted_tweets_limited_merge_list(ListEnoughTweets, EnoughNb),
                     Page).
-endif.


%% @doc Returns the given page of the tweets of the given user.
%% UserId must be valid user id.
-spec get_tweets(datamap_t(), common:user_id_t(), common:page_t()) -> common:tweets_t().
get_tweets(DataMap, UserId, 1) ->
    ?assert(is_datamap_t(DataMap)),
    ?assert(common:is_user_id_t(UserId)),
    ?assert(maps:is_key(UserId, DataMap)),
    %% Get the page from the top of the tweets list.
    {{Tweets, _LengthTweets,
      _PageRefs},
     _Subscriptions} = maps:get(UserId, DataMap),
    lists:sublist(Tweets, ?PAGE_SIZE);
get_tweets(DataMap, UserId, Page) ->
    ?assert(is_datamap_t(DataMap)),
    ?assert(common:is_user_id_t(UserId)),
    ?assert(maps:is_key(UserId, DataMap)),
    ?assert(common:is_page_t(Page)),
    %% Get the page from the page refs list.
    {{_Tweets, LengthTweets,
      PageRefs},
     _Subscriptions} = maps:get(UserId, DataMap),
    NbSkip = common:page_to_nb_skip(Page),
    if
        NbSkip < LengthTweets ->
            Mod = LengthTweets rem ?PAGE_SIZE,
            if
                Mod == 0 ->  % page starts exactly in a PageRefs item
                    ?assert(length(PageRefs) >= Page),
                    lists:sublist(lists:nth(Page, PageRefs), ?PAGE_SIZE);
                true ->
                    ?assert(length(PageRefs) >= Page - 1),
                    lists:sublist(lists:nth(Page - 1, PageRefs), ?PAGE_SIZE - Mod + 1, ?PAGE_SIZE)
            end;
        true ->  % not enough tweets
            []
    end.


%% @doc Returns true iff X is a valid datamap_t.
-spec is_datamap_t(any()) -> boolean().
is_datamap_t(X) ->
    if
        is_map(X) ->
            maps:fold(
              fun(Key, Value, Acc) ->
                      try
                          {{Tweets, LengthTweets,
                            PageRefs},
                           Subscriptions} = Value,
                          Acc andalso common:is_user_id_t(Key)
                              andalso common:is_tweets_t(Tweets, Key)
                              andalso (length(Tweets) == LengthTweets)
                              andalso (build_pagerefs(Tweets, LengthTweets) == PageRefs)
                              andalso is_list(Subscriptions)
                              andalso lists:all(fun common:is_user_id_t/1, Subscriptions)
                      catch
                          _Exception:_Reason -> false
                      end
              end,
              true,
              X);
        true ->
            false
    end.
