%%% @doc This is a simple (and inefficient) implementation of the project,
%%% using one centralized server.
%%%
%%% It will create one "server" actor that contains all internal state in data_t type
%%% (users, their subscriptions, and their tweets).
%%%
%%% @author Janwillem Swalens [http://soft.vub.ac.be/soft/members/jswalens]
%%% @author Olivier Pirson [http://www.opimedia.be/]
%%% @copyright GPLv3 --- 2018 Olivier Pirson
%%% @version May 9, 2018

-module(server_centralized).

-export([data_actor/1,
         initialize/0]).



%% @doc The data actor works like a small database
%% and encapsulates all state of this implementation.
-spec data_actor(data:data_t()) -> no_return().
data_actor(Data) ->
    receive
        {Sender, get_tweets, UserId, Page} ->
            Sender ! data:get_tweets(Data, UserId, Page),
            data_actor(Data);

        {Sender, get_timeline, UserId, Page} ->
            Sender ! data:get_timeline(Data, UserId, Page),
            data_actor(Data);

        {Sender, tweet, UserId, TweetContent} ->
            {NewData, Timestamp} = data:add_tweet(Data, UserId, TweetContent),
            Sender ! Timestamp,
            data_actor(NewData);

        {Sender, subscribe, UserId, UserIdToSubscribeTo} ->
            NewData = data:add_subscription_to_user(Data, UserId, UserIdToSubscribeTo),
            Sender ! ok,
            data_actor(NewData);

        {Sender, register_user} ->
            {NewData, NewUserId} = data:add_new_user(Data),
            Sender ! {NewUserId, self()},
            data_actor(NewData)
    end.


%% @doc Starts the server data_actor
%% and returns its pid.
-spec initialize() -> pid().
initialize() ->
    ServerPid = spawn_link(?MODULE, data_actor, [data:create()]),
    ServerPid.
