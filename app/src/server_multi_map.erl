%%% @doc This is an improved implementation (with a better data structure) of the project,
%%% using multiple servers.
%%%
%%% It will create ?NB_DATA_ACTOR "server" actors
%%% that contains all internal state in datamultimap_t type
%%% (users, their subscriptions, and their tweets).
%%%
%%% @author Olivier Pirson [http://www.opimedia.be/]
%%% @copyright GPLv3 --- 2018 Olivier Pirson
%%% @version May 15, 2018

-module(server_multi_map).

-include_lib("config.hrl").

-export([data_actor/1,
         initialize/0,
         i_to_data_actor_pid/2,
         timer_actor/2,
         user_id_actor/1]).



%% @doc The data actor works like a small database
%% and encapsulates all state of this implementation.
-spec data_actor(datamultimap:datamultimap_t()) -> no_return().
data_actor(DataMultiMap) ->
    receive
        ServerPids ->
            data_actor_loop_(datamultimap:set_other_server_pids(DataMultiMap, ServerPids, self()))
    end.

-spec data_actor_loop_(datamultimap:datamultimap_t()) -> no_return().
data_actor_loop_(DataMultiMap) ->
    receive
        {Sender, get_tweets, UserId, Page} ->
            Sender ! datamultimap:get_tweets(DataMultiMap, UserId, Page),
            data_actor_loop_(DataMultiMap);

        {Sender, get_timeline, UserId, Page} ->
            Sender ! datamultimap:get_timeline(DataMultiMap, UserId, Page),
            data_actor_loop_(DataMultiMap);

        {Sender, tweet, UserId, TweetContent} ->
            {NewDataMultiMap, NewTweet} =
                datamultimap:add_tweet(DataMultiMap, UserId, TweetContent),
            Sender ! common:get_tweet_time(NewTweet),
            data_actor_loop_(NewDataMultiMap);

        {Sender, subscribe, UserId, UserIdToSubscribeTo} ->
            NewDataMultiMap =
                datamultimap:add_subscription_to_user(DataMultiMap, UserId, UserIdToSubscribeTo),
            Sender ! ok,
            data_actor_loop_(NewDataMultiMap);

        {Sender, register_user} ->
            {NewDataMultiMap, NewUserId} = datamultimap:add_new_user(DataMultiMap),
            Sender ! {NewUserId, self()},
            data_actor_loop_(NewDataMultiMap);


        {update_recent_tweets, OtherRecentTweetsMap} ->
            data_actor_loop_(datamultimap:append_recent_tweets_map(DataMultiMap, OtherRecentTweetsMap));

        send_recent_tweets ->
            {OtherServerPids, DataMap, RecentTweetsMap} = DataMultiMap,
            if
                RecentTweetsMap /= [] ->
                    lists:foreach(fun(OtherServerPid) ->
                                          OtherServerPid ! {update_recent_tweets, RecentTweetsMap}
                                  end,
                                  OtherServerPids)
            end,
            data_actor_loop_({OtherServerPids, DataMap, datamultimap:empty_recent_tweets()})
    end.


%% @doc Returns the pid corresponding of the index i (in fact i modulo the length of the list).
-spec i_to_data_actor_pid(non_neg_integer(), [pid()]) -> pid().
i_to_data_actor_pid(I, ServerPids) ->
    common:mod_get(I, ServerPids, ?NB_DATA_ACTOR).


%% @doc A data actor that responds by successive user id: 0, 1, 2...
-spec timer_actor([pid()], [pid()]) -> no_return().
timer_actor(ServerPids, CurrentServerPids) ->
    hd(CurrentServerPids) ! send_recent_tweets,
    NextServerPids = tl(CurrentServerPids),
    timer:apply_after(?TIMER_DELAY, ?MODULE, timer_actor,
                      [ServerPids, if
                                       NextServerPids == [] -> ServerPids;
                                       true -> NextServerPids
                                   end]).


%% @doc A data actor that responds by successive user id: 0, 1, 2...
-spec user_id_actor(non_neg_integer()) -> no_return().
user_id_actor(NextUserId) ->
    receive
        restart ->
            user_id_actor(0);
        Sender ->
            Sender ! NextUserId,
            user_id_actor(NextUserId + 1)
    end.



%% @doc Starts ?NB_DATA_ACTOR servers data_actor
%% and returns the list of pids.
%% Starts also the user_id_actor server
%% and the timer_actor server.
-spec initialize() -> [pid()].
initialize() ->
    %% Start the actor for deal with user ids.
    Pid = spawn_link(?MODULE, user_id_actor, [0]),
    register(user_id_actor_name, Pid),

    %% Start data actors.
    ServerPids = lists:map(fun(_) ->
                                   spawn_link(?MODULE, data_actor, [datamultimap:create()])
                           end,
                           lists:seq(1, ?NB_DATA_ACTOR)),

    %% Send list to each of them.
    lists:foreach(fun(ServerPid) ->
                          ServerPid ! ServerPids
                  end, ServerPids),

    %% Start the timer to periodically update other servers with new tweets.
    timer:apply_after(?TIMER_DELAY, ?MODULE, timer_actor, [ServerPids, ServerPids]),

    ServerPids.
