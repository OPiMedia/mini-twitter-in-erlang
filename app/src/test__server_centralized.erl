%%% @doc Unit tests for server_centralized.erl.
%%%
%%% @author Olivier Pirson [http://www.opimedia.be/]
%%% @copyright GPLv3 --- 2018 Olivier Pirson
%%% @version May 15, 2018

-module(test__server_centralized).

-include_lib("eunit/include/eunit.hrl").

-include("config.hrl").
-include("debug.hrl").
-include("test.hrl").



msg_info_test() ->
    ct:print("START tests in ~s~n", [?FILE]).



%
% Tests functions of the server_centralized implementation.
%

get_timeline__without_following__test() ->
    NbUser = 123,
    ServerPid = server_centralized:initialize(),
    test__server:add_users(ServerPid, NbUser),
    lists:foreach(fun(UserId) ->
                          ?assertEqual([], server:get_timeline(ServerPid, UserId, 1)),
                          ?assertEqual([], server:get_timeline(ServerPid, UserId, 2)),
                          ?assertEqual([], server:get_timeline(ServerPid, UserId, 3)),
                          ?assertEqual([], server:get_timeline(ServerPid, UserId, 4))
                  end,
                  lists:seq(0, NbUser - 1)),

    test__server:add_tweets(ServerPid, 2, "Tweet from user 2 #", 3),

    ?assert(?PAGE_SIZE >= 3),
    lists:foreach(fun(UserId) ->
                          if
                              UserId == 2 ->
                                  [{tweet, 2, _Timestamp2_0, "Tweet from user 2 #2"},
                                   {tweet, 2, _Timestamp2_1, "Tweet from user 2 #1"},
                                   {tweet, 2, _Timestamp2_2, "Tweet from user 2 #0"}]
                                      = server:get_timeline(ServerPid, 2, 1);
                              true ->
                                  ?assertEqual([], server:get_timeline(ServerPid, UserId, 1))
                          end,
                          ?assertEqual([], server:get_timeline(ServerPid, UserId, 2)),
                          ?assertEqual([], server:get_timeline(ServerPid, UserId, 3)),
                          ?assertEqual([], server:get_timeline(ServerPid, UserId, 4))
                  end,
                  lists:seq(0, NbUser - 1)),

    test__server:add_tweets(ServerPid, 0, "Tweet from user 0 #", 1),
    test__server:add_tweets(ServerPid, 5, "Tweet from user 5 #", ?PAGE_SIZE*3),

    ?assertEqual([], server:get_timeline(ServerPid, 1, 1)),
    ?assertEqual([], server:get_timeline(ServerPid, 3, 1)),
    ?assertEqual([], server:get_timeline(ServerPid, 4, 1)),
    lists:foreach(fun(UserId) ->
                          ?assertEqual([], server:get_timeline(ServerPid, UserId, 1)),
                          ?assertEqual([], server:get_timeline(ServerPid, UserId, 2)),
                          ?assertEqual([], server:get_timeline(ServerPid, UserId, 3)),
                          ?assertEqual([], server:get_timeline(ServerPid, UserId, 4))
                  end,
                  lists:seq(6, NbUser - 1)),

    [{tweet, 0, Timestamp0_0, "Tweet from user 0 #0"}] = server:get_timeline(ServerPid, 0, 1),

    [{tweet, 2, Timestamp2_2, "Tweet from user 2 #2"},
     {tweet, 2, Timestamp2_1, "Tweet from user 2 #1"},
     {tweet, 2, Timestamp2_0, "Tweet from user 2 #0"}]
        = server:get_timeline(ServerPid, 2, 1),

    [{tweet, 5, Timestamp5_2, _},
     {tweet, 5, Timestamp5_1, _},
     {tweet, 5, Timestamp5_0, _}
    | RemainTweets5]
        = server:get_timeline(ServerPid, 5, 1),

    ?assertEqual(?PAGE_SIZE - 3, length(RemainTweets5)),

    ?assert(Timestamp2_0 < Timestamp2_1),
    ?assert(Timestamp2_1 < Timestamp2_2),
    ?assert(Timestamp2_2 < Timestamp0_0),
    ?assert(Timestamp0_0 < Timestamp5_0),
    ?assert(Timestamp5_0 < Timestamp5_1),
    ?assert(Timestamp5_1 < Timestamp5_2),

    lists:foreach(fun(UserId) ->
                          lists:foreach(fun(Page) ->
                                                ?assertEqual(
                                                   server:get_tweets(ServerPid, UserId, Page),
                                                   server:get_timeline(ServerPid, UserId, Page))
                                        end,
                                        lists:seq(1, 13))
                  end,
                  lists:seq(0, NbUser - 1)).


get_timeline__with_following__test() ->
    ServerPid = server_centralized:initialize(),
    test__server:add_users(ServerPid, 4),

    ?assertMatch([], server:get_timeline(ServerPid, 1, 1)),
    ?assertMatch([], server:get_timeline(ServerPid, 2, 1)),

    Before = common:current_time(),
    ?assertMatch(Timestamp when Before < Timestamp,
                                server:tweet(ServerPid, 1, "Tweet no. 1")),
    After = common:current_time(),

    ?assertMatch([{tweet, 1, Timestamp, "Tweet no. 1"}]
                 when (Before < Timestamp) andalso (Timestamp < After),
                      server:get_tweets(ServerPid, 1, 1)),
    ?assertMatch([], server:get_tweets(ServerPid, 2, 1)),

    ?assertMatch([{tweet, 1, Timestamp, "Tweet no. 1"}]
                 when (Before < Timestamp) andalso (Timestamp < After),
                      server:get_timeline(ServerPid, 1, 1)), % own tweets included in timeline
    ?assertMatch([], server:get_timeline(ServerPid, 2, 1)).


get_timeline__with_following2__test() ->
    ServerPid = server_centralized:initialize(),
    {timeout, 60,
     fun() ->
             Nbs = [7, 0, 5, 1, 2, 3, 4,
                    ?PAGE_SIZE   - 1, ?PAGE_SIZE,   ?PAGE_SIZE   + 1,
                    ?PAGE_SIZE*2 - 1, ?PAGE_SIZE*2, ?PAGE_SIZE*2 + 1,
                    ?PAGE_SIZE*3 - 1, ?PAGE_SIZE*3, ?PAGE_SIZE*3 + 1,
                    ?PAGE_SIZE*4 - 1, ?PAGE_SIZE*4, ?PAGE_SIZE*4 + 1,
                    ?PAGE_SIZE*5 - 1, ?PAGE_SIZE*5, ?PAGE_SIZE*5 + 1,
                    ?PAGE_SIZE*6 - 1, ?PAGE_SIZE*6, ?PAGE_SIZE*6 + 1],
             test__server:add_users_tweets(ServerPid, Nbs),
             test__server:add_subscriptions(ServerPid,
                                            [{0, 0},
                                             {1, 10},
                                             {2, 3}, {2, 1001},
                                             {3, 2}, {3, 3}, {3, 4}, {3, 1002},
                                             {15, 17}, {15, 19}, {15, 14},
                                             {16, 17}, {16, 19}, {16, 14}]),

             lists:foreach(
               fun(I) ->
                       Nb = lists:nth(I, Nbs),
                       AllTweets = test:get_timeline_all(ServerPid, I - 1),
                       LengthAllTweets = length(AllTweets),
                       ?assert(Nb =< LengthAllTweets),

                       lists:foreach(
                         fun(Page) ->
                                 Tweets = server:get_timeline(ServerPid, I - 1, Page),
                                 PageNb = min(?PAGE_SIZE,
                                              max(0,
                                                  LengthAllTweets - ?PAGE_SIZE * (Page - 1))),
                                 ?assert(PageNb == length(Tweets)),
                                 ?assertEqual(lists:sort(fun common:is_ordonned_tweet/2, Tweets),
                                              Tweets),

                                 CorrectTweets
                                     = try
                                           lists:sublist(AllTweets, ?PAGE_SIZE * (Page - 1) + 1,
                                                         ?PAGE_SIZE)
                                       catch
                                           _Exception:_Reason -> []
                                       end,
                                 ?assertEqual(CorrectTweets, Tweets)
                         end,
                         lists:seq(1, 10))
               end,
               lists:seq(1, length(Nbs)))
     end}.


get_tweets__test() ->
    NbUser = 321,
    ServerPid = server_centralized:initialize(),
    test__server:add_users(ServerPid, NbUser),
    lists:foreach(fun(UserId) ->
                          ?assertEqual([], server:get_tweets(ServerPid, UserId, 1)),
                          ?assertEqual([], server:get_tweets(ServerPid, UserId, 2)),
                          ?assertEqual([], server:get_tweets(ServerPid, UserId, 3)),
                          ?assertEqual([], server:get_tweets(ServerPid, UserId, 4))
                  end,
                  lists:seq(0, NbUser - 1)),

    test__server:add_tweets(ServerPid, 2, "Tweet from user 2 #", 3),

    ?assert(?PAGE_SIZE >= 3),
    lists:foreach(fun(UserId) ->
                          if
                              UserId == 2 ->
                                  [{tweet, 2, _Timestamp2_0, "Tweet from user 2 #2"},
                                   {tweet, 2, _Timestamp2_1, "Tweet from user 2 #1"},
                                   {tweet, 2, _Timestamp2_2, "Tweet from user 2 #0"}]
                                      = server:get_tweets(ServerPid, 2, 1);
                              true ->
                                  ?assertEqual([], server:get_tweets(ServerPid, UserId, 1))
                          end,
                          ?assertEqual([], server:get_tweets(ServerPid, UserId, 2)),
                          ?assertEqual([], server:get_tweets(ServerPid, UserId, 3)),
                          ?assertEqual([], server:get_tweets(ServerPid, UserId, 4))
                  end,
                  lists:seq(0, NbUser - 1)),

    test__server:add_tweets(ServerPid, 0, "Tweet from user 0 #", 1),
    test__server:add_tweets(ServerPid, 5, "Tweet from user 5 #", ?PAGE_SIZE*3),

    ?assertEqual([], server:get_tweets(ServerPid, 1, 1)),
    ?assertEqual([], server:get_tweets(ServerPid, 3, 1)),
    ?assertEqual([], server:get_tweets(ServerPid, 4, 1)),
    lists:foreach(fun(UserId) ->
                          ?assertEqual([], server:get_tweets(ServerPid, UserId, 1)),
                          ?assertEqual([], server:get_tweets(ServerPid, UserId, 2)),
                          ?assertEqual([], server:get_tweets(ServerPid, UserId, 3)),
                          ?assertEqual([], server:get_tweets(ServerPid, UserId, 4))
                  end,
                  lists:seq(6, NbUser - 1)),

    [{tweet, 0, Timestamp0_0, "Tweet from user 0 #0"}] = server:get_tweets(ServerPid, 0, 1),

    [{tweet, 2, Timestamp2_2, "Tweet from user 2 #2"},
     {tweet, 2, Timestamp2_1, "Tweet from user 2 #1"},
     {tweet, 2, Timestamp2_0, "Tweet from user 2 #0"}]
        = server:get_tweets(ServerPid, 2, 1),

    [{tweet, 5, Timestamp5_2, _},
     {tweet, 5, Timestamp5_1, _},
     {tweet, 5, Timestamp5_0, _}
    | RemainTweets5]
        = server:get_tweets(ServerPid, 5, 1),

    ?assertEqual(?PAGE_SIZE - 3, length(RemainTweets5)),

    ?assert(Timestamp2_0 < Timestamp2_1),
    ?assert(Timestamp2_1 < Timestamp2_2),
    ?assert(Timestamp2_2 < Timestamp0_0),
    ?assert(Timestamp0_0 < Timestamp5_0),
    ?assert(Timestamp5_0 < Timestamp5_1),
    ?assert(Timestamp5_1 < Timestamp5_2).


initialize__test() ->
    ServerPid = server_centralized:initialize(),
    ?assert(is_pid(ServerPid)),
    ?assertNotEqual(self(), ServerPid).


register_user__test() ->
    ServerPid = server_centralized:initialize(),

    {UserId0, Pid0} = server:register_user(ServerPid),
    ?assertEqual(0, UserId0),
    ?assert(is_pid(Pid0)),

    {UserId1, Pid1} = server:register_user(ServerPid),
    ?assertEqual(1, UserId1),
    ?assert(is_pid(Pid1)),

    {UserId2, Pid2} = server:register_user(ServerPid),
    ?assertEqual(2, UserId2),
    ?assert(is_pid(Pid2)),

    {UserId3, Pid3} = server:register_user(ServerPid),
    ?assertEqual(3, UserId3),
    ?assert(is_pid(Pid3)),

    test__server:add_users(ServerPid, 666).


subscribe__test() ->
    NbUser = 10,
    ServerPid = server_centralized:initialize(),
    test__server:add_users(ServerPid, NbUser),
    lists:foreach(fun(UserId) ->
                          ?assertEqual([], test:get_tweets_all(ServerPid, UserId))
                  end,
                  lists:seq(0, NbUser - 1)),

    test__server:add_tweets(ServerPid, 2, "Tweet from user 2 #", 3),
    test__server:add_tweets(ServerPid, 5, "Tweet from user 5 #", 2),
    test__server:add_tweets(ServerPid, 9, "Tweet from user 9 #", ?PAGE_SIZE*4),
    ?assert(lists:all(fun(UserId) ->
                              ?assertEqual(test:get_tweets_all(ServerPid, UserId),
                                           test:get_timeline_all(ServerPid, UserId)),
                              lists:member(UserId, [2, 5, 9])
                                  orelse (test:get_timeline_all(ServerPid, UserId) == [])
                      end,
                      lists:seq(0, NbUser - 1))),

    [{tweet, 2, _Timestamp2_2, "Tweet from user 2 #2"},
     {tweet, 2, _Timestamp2_1, "Tweet from user 2 #1"},
     {tweet, 2, _Timestamp2_0, "Tweet from user 2 #0"}]
        = server:get_timeline(ServerPid, 2, 1),

    [{tweet, 5, _Timestamp5_1, "Tweet from user 5 #1"},
     {tweet, 5, _Timestamp5_0, "Tweet from user 5 #0"}]
        = server:get_timeline(ServerPid, 5, 1),

    [{tweet, 9, _Timestamp9_2, _},
     {tweet, 9, _Timestamp9_1, _},
     {tweet, 9, _Timestamp9_0, _}
    | RemainTweets9]
        = server:get_timeline(ServerPid, 9, 1),

    %% Subscribes 2 to 5
    ?assertEqual(ok, server:subscribe(ServerPid, 2, 5)),

    ?assert(lists:all(fun(UserId) ->
                              (UserId == 2)
                                  orelse (test:get_timeline_all(ServerPid, UserId)
                                          == test:get_tweets_all(ServerPid, UserId))
                      end,
                      lists:seq(0, NbUser - 1))),


    [{tweet, 5, _Timestamp5_1, "Tweet from user 5 #1"},  % from user 5
     {tweet, 5, _Timestamp5_0, "Tweet from user 5 #0"},
     {tweet, 2, _Timestamp2_2, "Tweet from user 2 #2"}
    | RemainTweets2]
        = server:get_timeline(ServerPid, 2, 1),
    ?assertEqual(min(?PAGE_SIZE, 3 + 2), length(RemainTweets2) + 3),

    [{tweet, 5, _Timestamp5_1, "Tweet from user 5 #1"},
     {tweet, 5, _Timestamp5_0, "Tweet from user 5 #0"}]
        = server:get_timeline(ServerPid, 5, 1),

    [{tweet, 9, _Timestamp9_2, _},
     {tweet, 9, _Timestamp9_1, _},
     {tweet, 9, _Timestamp9_0, _}
    | RemainTweets9]
        = server:get_timeline(ServerPid, 9, 1),

    %% Subscribes 5 to 2
    ?assertEqual(ok, server:subscribe(ServerPid, 5, 2)),

    ?assert(lists:all(fun(UserId) ->
                              lists:member(UserId, [2, 5])
                                  orelse (test:get_timeline_all(ServerPid, UserId)
                                          == test:get_tweets_all(ServerPid, UserId))
                      end,
                      lists:seq(0, NbUser - 1))),


    [{tweet, 5, _Timestamp5_1, "Tweet from user 5 #1"},  % from user 5
     {tweet, 5, _Timestamp5_0, "Tweet from user 5 #0"},
     {tweet, 2, _Timestamp2_2, "Tweet from user 2 #2"}
    | RemainTweets2]
        = server:get_timeline(ServerPid, 2, 1),
    ?assertEqual(min(?PAGE_SIZE, 3 + 2), length(RemainTweets2) + 3),

    [{tweet, 5, _Timestamp5_1, "Tweet from user 5 #1"},
     {tweet, 5, _Timestamp5_0, "Tweet from user 5 #0"},
     {tweet, 2, _Timestamp2_2, "Tweet from user 2 #2"}  % from user 2
    | RemainTweets5]
        = server:get_timeline(ServerPid, 5, 1),
    ?assertEqual(min(?PAGE_SIZE, 3 + 2), length(RemainTweets5) + 3),

    [{tweet, 9, _Timestamp9_2, _},
     {tweet, 9, _Timestamp9_1, _},
     {tweet, 9, _Timestamp9_0, _}
    | RemainTweets9]
        = server:get_timeline(ServerPid, 9, 1),

    %%
    test__server:add_tweets(ServerPid, 2, "New tweet from user 2 #", 1),

    ?assert(lists:all(fun(UserId) ->
                              lists:member(UserId, [2, 5])
                                  orelse (test:get_timeline_all(ServerPid, UserId)
                                          == test:get_tweets_all(ServerPid, UserId))
                      end,
                      lists:seq(0, NbUser - 1))),

    [{tweet, 2, _NewTimestamp2_2, "New tweet from user 2 #0"},
     {tweet, 5, _Timestamp5_1, "Tweet from user 5 #1"},  % from user 5
     {tweet, 5, _Timestamp5_0, "Tweet from user 5 #0"}
    | NewRemainTweets2]
        = server:get_timeline(ServerPid, 2, 1),
    ?assertEqual(min(?PAGE_SIZE, 4 + 2), length(NewRemainTweets2) + 3),

    [{tweet, 2, _NewTimestamp2_0, "New tweet from user 2 #0"},  % new from user 2
     {tweet, 5, _Timestamp5_1, "Tweet from user 5 #1"},
     {tweet, 5, _Timestamp5_0, "Tweet from user 5 #0"}
    | NewRemainTweets5]
        = server:get_timeline(ServerPid, 5, 1),
    ?assertEqual(min(?PAGE_SIZE, 4 + 2), length(NewRemainTweets5) + 3),

    [{tweet, 9, _Timestamp9_2, _},
     {tweet, 9, _Timestamp9_1, _},
     {tweet, 9, _Timestamp9_0, _}
    | RemainTweets9]
        = server:get_timeline(ServerPid, 9, 1),

    %%
    test__server:add_tweets(ServerPid, 5, "New tweet from user 5 #", 1),

    [{tweet, 9, _Timestamp9_2, _},
     {tweet, 9, _Timestamp9_1, _},
     {tweet, 9, _Timestamp9_0, _}
    | RemainTweets9]
        = server:get_timeline(ServerPid, 9, 1),

    %% Subscribes 9 to 5
    ?assertEqual(ok, server:subscribe(ServerPid, 9, 5)),

    [{tweet, 5, _NewTimestamp5_0, "New tweet from user 5 #0"},
     {tweet, 9, _NewTimestamp9_1, _},
     {tweet, 9, _NewTimestamp9_0, _}
    | _NewRemainTweets9]
        = server:get_timeline(ServerPid, 9, 1),

    ?assert(lists:all(fun(UserId) ->
                              lists:member(UserId, [2, 5, 9])
                                  orelse (test:get_timeline_all(ServerPid, UserId)
                                          == test:get_tweets_all(ServerPid, UserId))
                      end,
                      lists:seq(0, NbUser - 1))),

    AllTweets9 = lists:sort(fun common:is_ordonned_tweet/2,
                            lists:append(test:get_tweets_all(ServerPid, 9),
                                         test:get_tweets_all(ServerPid, 5))),
    lists:foreach(fun(Page) ->
                          ?assertEqual(common:list_page(AllTweets9, Page),
                                       server:get_timeline(ServerPid, 9, Page))
                  end,
                  lists:seq(1, 10)).


tweet__test() ->
    NbUser = 42,
    ServerPid = server_centralized:initialize(),
    test__server:add_users(ServerPid, NbUser),
    lists:foreach(fun(UserId) ->
                          ?assertEqual([], test:get_tweets_all(ServerPid, UserId))
                  end,
                  lists:seq(0, NbUser - 1)),

    test__server:add_tweets(ServerPid, 2, "Tweet from user 2 #", 3),

    lists:foreach(fun(UserId) ->
                          if
                              UserId == 2 ->
                                  [{tweet, 2, _Timestamp2_0, "Tweet from user 2 #2"},
                                   {tweet, 2, _Timestamp2_1, "Tweet from user 2 #1"},
                                   {tweet, 2, _Timestamp2_2, "Tweet from user 2 #0"}]
                                      = test:get_tweets_all(ServerPid, 2);
                              true ->
                                  ?assertEqual([], test:get_tweets_all(ServerPid, UserId))
                          end
                  end,
                  lists:seq(0, NbUser - 1)),

    test__server:add_tweets(ServerPid, 0, "Tweet from user 0 #", 1),
    test__server:add_tweets(ServerPid, 5, "Tweet from user 5 #", ?PAGE_SIZE*3),

    ?assertEqual([], test:get_tweets_all(ServerPid, 1)),
    ?assertEqual([], test:get_tweets_all(ServerPid, 3)),
    ?assertEqual([], test:get_tweets_all(ServerPid, 4)),
    lists:foreach(fun(UserId) ->
                          ?assertEqual([], test:get_tweets_all(ServerPid, UserId))
                  end,
                  lists:seq(6, NbUser - 1)),

    [{tweet, 0, Timestamp0_0, "Tweet from user 0 #0"}] = test:get_tweets_all(ServerPid, 0),

    [{tweet, 2, Timestamp2_2, "Tweet from user 2 #2"},
     {tweet, 2, Timestamp2_1, "Tweet from user 2 #1"},
     {tweet, 2, Timestamp2_0, "Tweet from user 2 #0"}]
        = test:get_tweets_all(ServerPid, 2),

    [{tweet, 5, Timestamp5_3, _},
     {tweet, 5, Timestamp5_2, _},
     {tweet, 5, Timestamp5_1, _},
     {tweet, 5, Timestamp5_0, _}
    | RemainTweets5]
        = test:get_tweets_all(ServerPid, 5),

    ?assertEqual(?PAGE_SIZE*3 - 4, length(RemainTweets5)),

    ?assert(Timestamp2_0 < Timestamp2_1),
    ?assert(Timestamp2_1 < Timestamp2_2),
    ?assert(Timestamp2_2 < Timestamp0_0),
    ?assert(Timestamp0_0 < Timestamp5_0),
    ?assert(Timestamp5_0 < Timestamp5_1),
    ?assert(Timestamp5_1 < Timestamp5_2),
    ?assert(Timestamp5_2 < Timestamp5_3).



%
% Tests server with the server_centralized implementation.
%

server__test() ->
    ServerPid = server_centralized:initialize(),
    ?NOT_COVERED.
