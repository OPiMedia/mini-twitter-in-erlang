%%% @doc This is an improved implementation (with a better data structure) of the project,
%%% using one centralized server.
%%%
%%% It will create one "server" actor that contains all internal state in datamap_t type
%%% (users, their subscriptions, and their tweets).
%%%
%%% @author Olivier Pirson [http://www.opimedia.be/]
%%% @copyright GPLv3 --- 2018 Olivier Pirson
%%% @version May 15, 2018

-module(server_centralized_map).

-export([data_actor/1,
         initialize/0]).



%% @doc The data actor works like a small database
%% and encapsulates all state of this implementation.
-spec data_actor(datamap:datamap_t()) -> no_return().
data_actor(DataMap) ->
    receive
        {Sender, get_tweets, UserId, Page} ->
            Sender ! datamap:get_tweets(DataMap, UserId, Page),
            data_actor(DataMap);

        {Sender, get_timeline, UserId, Page} ->
            Sender ! datamap:get_timeline(DataMap, UserId, Page),
            data_actor(DataMap);

        {Sender, tweet, UserId, TweetContent} ->
            {NewDataMap, {_tweet, _UserId, Timestamp, _TweetContent}} =
                datamap:add_tweet(DataMap, UserId, TweetContent),
            Sender ! Timestamp,
            data_actor(NewDataMap);

        {Sender, subscribe, UserId, UserIdToSubscribeTo} ->
            NewDataMap = datamap:add_subscription_to_user(DataMap, UserId, UserIdToSubscribeTo),
            Sender ! ok,
            data_actor(NewDataMap);

        {Sender, register_user} ->
            {NewDataMap, NewUserId} = datamap:add_new_user(DataMap),
            Sender ! {NewUserId, self()},
            data_actor(NewDataMap)
    end.


%% @doc Starts the server data_actor
%% and returns its pid.
-spec initialize() -> pid().
initialize() ->
    ServerPid = spawn_link(?MODULE, data_actor, [datamap:create()]),
    ServerPid.
