%%% @doc Data structure to deal with multiple processes,
%%% and associated functions.
%%%
%%% @author Olivier Pirson [http://www.opimedia.be/]
%%% @copyright GPLv3 --- 2018 Olivier Pirson
%%% @version May 15, 2018

-module(datamultimap).

-include_lib("stdlib/include/assert.hrl").

-include_lib("config.hrl").
-include_lib("debug.hrl").

-export([add_new_user/1,
         add_subscription_to_user/3,
         add_tweet/3,
         append_recent_tweets_map/2,
         create/0,
         empty_recent_tweets/0,
         get_datamap/1,
         get_other_server_pids/1,
         get_timeline/3,
         get_tweets/3,
         is_datamultimap_t/1,
         is_recent_tweets_map_t/1,
         set_other_server_pids/3]).

-export_type([datamultimap_t/0,
              recent_tweets_map_t/0]).



%% @type datamultimap_t(). Data structure for all data (users and tweets)
%% with multiple processes implementation.
%% {list of server pids different that the current actor,
%%  DataMap,
%%  map recent_tweets_map_t() for recent tweets added on this server not already copied on others}
-type datamultimap_t() :: {[pid()],
                           datamap:datamap_t(),
                           #{common:user_id_t() => [common:tweets_t()]}}.

%% @type recent_tweets_map_t(). Map:
%%   user id => list of tweets
-type recent_tweets_map_t() :: #{common:user_id_t() => [common:tweets_t()]}.


%% @doc Returns a new structure with the a new user added and the id of this new user.
%% Require the user_id_actor_name actor to receive a new user id.
-spec add_new_user(datamultimap_t()) -> {datamultimap_t(), common:user_id_t()}.
add_new_user(DataMultiMap) ->
    ?assert(is_datamultimap_t(DataMultiMap)),
    user_id_actor_name ! self(),
    {OtherServerPids, DataMap, RecentTweetsMap} = DataMultiMap,
    receive
        NewUserId ->
            NewDataMap = datamap:add_user(DataMap, NewUserId),
            {{OtherServerPids, NewDataMap, RecentTweetsMap},
             NewUserId}
    end.


%% @doc Returns a new structure with the user UserIdToSubscribeTo followed by UserId.
%% If UserIdToSubscribeTo was already followed by UserId
%% or if UserId == UserIdToSubscribeTo
%% then returns Data.
%% UserId must be valid.
%% UserIdToSubscribeTo may be invalid but must be >= 0.
-spec add_subscription_to_user(datamultimap_t(), common:user_id_t(), common:user_id_t()) -> datamultimap_t().
add_subscription_to_user(DataMultiMap, UserId, UserIdToSubscribeTo) ->
    ?assert(is_datamultimap_t(DataMultiMap)),
    ?assert(common:is_user_id_t(UserId)),
    ?assert(maps:is_key(UserId, get_datamap(DataMultiMap))),
    ?assert(common:is_user_id_t(UserIdToSubscribeTo)),
    if
        UserIdToSubscribeTo /= UserId ->
            {OtherServerPids, DataMap, RecentTweetsMap} = DataMultiMap,
            {OtherServerPids,
             datamap:add_subscription_to_user(DataMap, UserId, UserIdToSubscribeTo),
             RecentTweetsMap};
        true ->
            DataMultiMap
    end.


%% @doc Returns a new structure with the tweet.
%% UserId must be valid user id.
-spec add_tweet(datamultimap_t(), common:user_id_t(), common:tweet_content_t()) -> {datamultimap_t(), common:tweet_t()}.
add_tweet(DataMultiMap, UserId, TweetContent) ->
    ?assert(is_datamultimap_t(DataMultiMap)),
    ?assert(common:is_user_id_t(UserId)),
    ?assert(maps:is_key(UserId, get_datamap(DataMultiMap))),
    ?assert(common:is_tweet_content_t(TweetContent)),
    {OtherServerPids, DataMap, RecentTweetsMap} = DataMultiMap,
    {NewDataMap, NewTweet} = datamap:add_tweet(DataMap, UserId, TweetContent),
    NewRecentTweetsMap = maps:put(UserId,
                                  [NewTweet | maps:get(UserId, RecentTweetsMap, [])],
                                  RecentTweetsMap),
    {{OtherServerPids, NewDataMap, NewRecentTweetsMap}, NewTweet}.


%% @doc Returns a new structure with the map of tweets added.
-spec append_recent_tweets_map(datamultimap_t(), recent_tweets_map_t()) -> datamultimap_t().
append_recent_tweets_map(DataMultiMap, OtherRecentTweetsMap) ->
    ?assert(is_datamultimap_t(DataMultiMap)),
    ?assert(is_recent_tweets_map_t(OtherRecentTweetsMap)),
    {OtherServerPids, DataMap, RecentTweetsMap} = DataMultiMap,
    TempTweetsMap = maps:map(fun(OtherUserId, OtherRecentTweets) ->
                                     {{Tweets, LengthTweets,
                                       _PageRefs},
                                      Subscriptions} =
                                         maps:get(OtherUserId, DataMap, datamap:empty_value()),
                                     NewTweets =
                                         common:sorted_tweets_merge(OtherRecentTweets, Tweets),
                                     NewLengthTweets = LengthTweets + length(OtherRecentTweets),
                                     {{NewTweets, NewLengthTweets,
                                       %% ???possible to use PageRefs to recompute pageref
                                       datamap:build_pagerefs(NewTweets, NewLengthTweets)},
                                      Subscriptions}
                             end,
                             OtherRecentTweetsMap),
    %%??? update_with ?
    {OtherServerPids, maps:merge(DataMap, TempTweetsMap), RecentTweetsMap}.


%% @doc Returns a empty data structure datamultimap_t.
-spec create() -> datamultimap_t().
create() ->
    {[], datamap:create(), empty_recent_tweets()}.


%% @doc Empty list for the temporary list of recent tweets.
-spec empty_recent_tweets() -> #{common:user_id_t() => [common:tweets_t()]}.
empty_recent_tweets() ->
    maps:new().


%% @doc Returns the datamap piece of the datamultimap.
-spec get_datamap(datamultimap_t()) -> datamap:datamap_t().
get_datamap(DataMultiMap) ->
    ?assert(is_datamultimap_t(DataMultiMap)),
    {_OtherServerPids, DataMap, _RecentTweetsMap} = DataMultiMap,
    DataMap.


%% @doc Returns the list of the pid servers, except the pid of the current server.
-spec get_other_server_pids(datamultimap_t()) -> [pid()].
get_other_server_pids(DataMultiMap) ->
    ?assert(is_datamultimap_t(DataMultiMap)),
    {OtherServerPids, _DataMap, _RecentTweetsMap} = DataMultiMap,
    OtherServerPids.


%% @doc Returns the given page of the timeline of the given user.
%% UserId must be valid user id.
%% Unknown followed users are ignored.
-spec get_timeline(datamultimap_t(), common:user_id_t(), common:page_t()) -> common:tweets_t().
get_timeline(DataMultiMap, UserId, Page) ->
    ?assert(is_datamultimap_t(DataMultiMap)),
    ?assert(common:is_user_id_t(UserId)),
    ?assert(maps:is_key(UserId, get_datamap(DataMultiMap))),
    ?assert(common:is_page_t(Page)),
    {_OtherServerPids, DataMap, _RecentTweetsMap} = DataMultiMap,
    datamap:get_timeline(DataMap, UserId, Page).


%% @doc Returns the given page of the tweets of the given user.
%% UserId must be valid user id.
-spec get_tweets(datamultimap_t(), common:user_id_t(), common:page_t()) -> common:tweets_t().
get_tweets(DataMultiMap, UserId, Page) ->
    ?assert(is_datamultimap_t(DataMultiMap)),
    ?assert(common:is_user_id_t(UserId)),
    ?assert(maps:is_key(UserId, get_datamap(DataMultiMap))),
    ?assert(common:is_page_t(Page)),
    {_OtherServerPids, DataMap, _RecentTweetsMap} = DataMultiMap,
    datamap:get_tweets(DataMap, UserId, Page).


%% @doc Returns true iff X is a valid datamultimap_t.
-spec is_datamultimap_t(any()) -> boolean().
is_datamultimap_t(X) ->
    try
        {OtherServerPids, DataMap, RecentTweetsMap} = X,
        is_list(OtherServerPids)
            andalso lists:all(fun is_pid/1, OtherServerPids)
            andalso datamap:is_datamap_t(DataMap)
            andalso is_recent_tweets_map_t(RecentTweetsMap)
    catch
        _Exception:_Reason -> false
    end.


%% @doc Returns true iff X is a valid recent_tweets_map_t.
-spec is_recent_tweets_map_t(any()) -> boolean().
is_recent_tweets_map_t(X) ->
    if
        is_map(X) ->
            maps:fold(
              fun(Key, Value, Acc) ->
                      Acc andalso common:is_user_id_t(Key)
                          andalso common:is_tweets_t(Value, Key)
              end,
              true,
              X);
        true ->
            false
    end.


%% @doc Returns the data structure
%% with the new list of server pids ServerPids different than CurrentServerPid.
-spec set_other_server_pids(datamultimap_t(), [pid()], pid()) -> datamultimap_t().
set_other_server_pids(DataMultiMap, ServerPids, CurrentServerPid) ->
    ?assert(is_datamultimap_t(DataMultiMap)),
    ?assert(lists:all(fun is_pid/1, ServerPids)),
    ?assert(is_pid(CurrentServerPid)),
    OtherServerPids = lists:filter(fun(ServerPid) ->
                                           ServerPid /= CurrentServerPid
                                   end, ServerPids),
    {_OldOtherServerPids, DataMap, RecentTweetsMap} = DataMultiMap,
    {OtherServerPids, DataMap, RecentTweetsMap}.
