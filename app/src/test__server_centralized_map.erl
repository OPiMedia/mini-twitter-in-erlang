%%% @doc Unit tests for server_centralized_map.erl.
%%%
%%% @author Olivier Pirson [http://www.opimedia.be/]
%%% @copyright GPLv3 --- 2018 Olivier Pirson
%%% @version May 15, 2018

-module(test__server_centralized_map).

-include_lib("eunit/include/eunit.hrl").

-include("config.hrl").
-include("debug.hrl").
-include("test.hrl").



msg_info_test() ->
    ct:print("START tests in ~s~n", [?FILE]).



%
% Tests functions of the server_centralized_map implementation
%

initialization_test() ->
    server_centralized_map:initialize().


register_user__test() ->
    ServerPid = initialization_test(),

    ?assertMatch({0, _ResponsePid1}, server:register_user(ServerPid)),
    ?assertMatch({1, _ResponsePid2}, server:register_user(ServerPid)),
    ?assertMatch({2, _ResponsePid3}, server:register_user(ServerPid)),
    ?assertMatch({3, _ResponsePid4}, server:register_user(ServerPid)).


init_for_test() ->
    ServerPid = initialization_test(),
    {0, ResponsePid1} = server:register_user(ServerPid),
    {1, ResponsePid2} = server:register_user(ServerPid),
    {2, ResponsePid3} = server:register_user(ServerPid),
    {3, ResponsePid4} = server:register_user(ServerPid),
    [ResponsePid1, ResponsePid2, ResponsePid3, ResponsePid4].



get_timeline__without_following__test() ->
    ResponsePids = init_for_test(),
    [ResponsePid1, ResponsePid2 | _ ] = ResponsePids,

    ?assertMatch([], server:get_timeline(ResponsePid1, 1, 1)),
    ?assertMatch([], server:get_timeline(ResponsePid2, 2, 1)),

    lists:foreach(fun({ResponsePid, UserId}) ->
                          ?assertMatch([], server:get_timeline(ResponsePid, UserId, 1))
                  end, [{ResponsePid, UserId}
                        || ResponsePid <- ResponsePids, UserId <- lists:seq(0, 3)]).


get_tweets__test() ->
    ResponsePids = init_for_test(),
    [ResponsePid1 | _ ] = ResponsePids,

    ?assertMatch([], server:get_tweets(ResponsePid1, 1, 1)),
    ?assertMatch([], server:get_tweets(ResponsePid1, 2, 1)),

    lists:foreach(fun({ResponsePid, UserId}) ->
                          ?assertMatch([], server:get_tweets(ResponsePid, UserId, 1))
                  end, [{ResponsePid, UserId}
                        || ResponsePid <- ResponsePids, UserId <- lists:seq(0, 3)]).


get_timeline__with_following__test() ->
    ResponsePids = init_for_test(),
    [ResponsePid1, ResponsePid2 | _ ] = ResponsePids,

    ?assertMatch([], server:get_timeline(ResponsePid1, 1, 1)),
    ?assertMatch([], server:get_timeline(ResponsePid2, 2, 1)),

    Before = common:current_time(),
    ?assertMatch(Timestamp when Before < Timestamp,
                                server:tweet(ResponsePid1, 1, "Tweet no. 1")),
    After = common:current_time(),

    ?assertMatch([{tweet, 1, Timestamp, "Tweet no. 1"}]
                 when (Before < Timestamp) andalso (Timestamp < After),
                      server:get_tweets(ResponsePid1, 1, 1)),
    ?assertMatch([], server:get_tweets(ResponsePid1, 2, 1)),

    ?assertMatch([{tweet, 1, Timestamp, "Tweet no. 1"}]
                 when (Before < Timestamp) andalso (Timestamp < After),
                      server:get_timeline(ResponsePid1, 1, 1)), % own tweets included in timeline
    ?assertMatch([], server:get_timeline(ResponsePid2, 2, 1)),

    ResponsePids. % no subscription


subscribe__test() ->
    [_ResponsePid1, ResponsePid2 | _ ] = get_timeline__with_following__test(),

    ?assertEqual(ok, server:subscribe(ResponsePid2, 2, 1)),

    ?assertMatch([{tweet, 1, _Timestamp, "Tweet no. 1"}], server:get_timeline(ResponsePid2, 2, 1)), % now there is a subscription

    server:tweet(ResponsePid2, 2, "Tweet no. 2"),
    ?assertMatch([{tweet, 2, Timestamp2, "Tweet no. 2"},
                  {tweet, 1, Timestamp1, "Tweet no. 1"}]
                 when (Timestamp2 > Timestamp1),
                      server:get_timeline(ResponsePid2, 2, 1)),
    done.
