%%% @doc Simple (and inefficient) data structure to save all users and tweets,
%%% and associated functions.
%%%
%%% @author Janwillem Swalens [http://soft.vub.ac.be/soft/members/jswalens]
%%% @author Olivier Pirson [http://www.opimedia.be/]
%%% @copyright GPLv3 --- 2018 Olivier Pirson
%%% @version May 10, 2018

-module(data).

-include_lib("stdlib/include/assert.hrl").

-include_lib("debug.hrl").

-export([add_new_user/1,
         add_subscription_to_user/3,
         add_tweet/3,
         create/0,
         empty_value/1,
         get_timeline/3,
         get_tweets/3,
         is_data_t/1]).


-export_type([data_t/0]).



%% @type data_t(). Simple and inefficient data structure for all data (users and tweets).
%%   List of {user, user id, list of tweets, set of followed users}.
-type data_t() :: [{user, common:user_id_t(), common:tweets_t(), sets:set(common:user_id_t())}].



%% @doc Returns a new structure with a new user added and the id of this new user.
-spec add_new_user(data_t()) -> {data_t(), common:user_id_t()}.
add_new_user(Data) ->
    ?assert(is_data_t(Data)),
    NewUserId = length(Data),
    ?assert(lists:all(fun({_user, UserId, _Tweets, _Subscriptions}) ->
                              UserId /= NewUserId
                      end,
                      Data)),
    NewData = Data ++ [empty_value(NewUserId)],
    {NewData, NewUserId}.


%% @doc Returns a new structure with the user UserIdToSubscribeTo followed by UserId.
%% If UserId == UserIdToSubscribeTo then returns Data.
%% If UserIdToSubscribeTo was already followed by UserId, then returns a copy of Data.
%% UserId and UserIdToSubscribeTo must be valid user id.
-spec add_subscription_to_user(data_t(), common:user_id_t(), common:user_id_t()) -> data_t().
add_subscription_to_user(Data, UserId, UserIdToSubscribeTo) ->
    ?assert(is_data_t(Data)),
    ?assert(common:is_user_id_t(UserId)),
    ?assert(UserId < length(Data)),
    ?assert(common:is_user_id_t(UserIdToSubscribeTo)),
    ?assert(UserIdToSubscribeTo < length(Data)),
    if
        UserIdToSubscribeTo /= UserId ->
            {_user, TweetUserId, Tweets, Subscriptions} = lists:nth(UserId + 1, Data),
            ?assertEqual(TweetUserId, UserId),
            NewUser = {user, UserId, Tweets, sets:add_element(UserIdToSubscribeTo, Subscriptions)},

            {UsersBefore, [_UserOld|UsersAfter]} = lists:split(UserId, Data),
            lists:append([UsersBefore, [NewUser | UsersAfter]]);
        true ->
            Data
    end.


%% @doc Returns a new structure with the tweet, and the tweet.
%% UserId must be valid user id.
-spec add_tweet(data_t(), common:user_id_t(), common:tweet_content_t()) -> {data_t(), common:tweet_time_t()}.
add_tweet(Data, UserId, TweetContent) ->
    ?assert(is_data_t(Data)),
    ?assert(common:is_user_id_t(UserId)),
    ?assert(UserId < length(Data)),
    ?assert(common:is_tweet_content_t(TweetContent)),
    {_user, _UserId, Tweets, Subscriptions} = lists:nth(UserId + 1, Data),
    Timestamp = common:current_time(),
    NewUser = {user, UserId, [{tweet, UserId, Timestamp, TweetContent} | Tweets], Subscriptions},

    {UsersBefore, [_UserOld|UsersAfter]} = lists:split(UserId, Data),
    {lists:append([UsersBefore, [NewUser | UsersAfter]]), Timestamp}.


%% @doc Returns a empty data structure data_t.
-spec create() -> data_t().
create() ->
    [].


%% @doc Empty value for the user UserId in the data_t() data structure.
-spec empty_value(common:user_id_t()) -> {user, common:user_id_t(), [], sets:set(common:user_id_t())}.
empty_value(UserId) ->
    {user, UserId, [], sets:new()}.


%% @doc Returns the given page of the timeline of the given user.
%% UserId must be valid user id.
%% Error if there are some unknown followed users.
-spec get_timeline(data_t(), common:user_id_t(), common:page_t()) -> common:tweets_t().
get_timeline(Data, UserId, Page) ->
    ?assert(is_data_t(Data)),
    ?assert(common:is_user_id_t(UserId)),
    ?assert(UserId < length(Data)),
    ?assert(common:is_page_t(Page)),
    {_user, _UserId, Tweets, Subscriptions} = lists:nth(UserId + 1, Data),
    UnsortedTweets =
        sets:fold(fun(OtherUserId, AllTweets) ->
                          {_user, _OtherUserId, OtherTweets, _Subscriptions}
                              = lists:nth(OtherUserId + 1, Data),
                          OtherTweets ++ AllTweets
                  end,
                  Tweets,
                  Subscriptions),
    %% SortedTweets = lists:reverse(lists:keysort(3, UnsortedTweets)),
    SortedTweets = lists:sort(fun common:is_ordonned_tweet/2, UnsortedTweets),
    common:list_page(SortedTweets, Page).


%% @doc Returns the given page of the tweets of the given user.
%% UserId may be invalid but must be >= 0.
-spec get_tweets(data_t(), common:user_id_t(), common:page_t()) -> common:tweets_t().
get_tweets(Data, UserId, Page) ->
    ?assert(is_data_t(Data)),
    ?assert(common:is_user_id_t(UserId)),
    ?assert(common:is_page_t(Page)),
    {_user, _UserId, Tweets, _Subscriptions} = lists:nth(UserId + 1, Data),
    common:list_page(Tweets, Page).


%% @doc Returns true iff X is a valid data_t.
-spec is_data_t(any()) -> boolean().
is_data_t(X) ->
    if
        is_list(X) ->
            lists:all(
              fun({I, Value}) ->
                      try
                          {user, UserId, Tweets, Subscriptions} = Value,
                          common:is_user_id_t(UserId)
                              andalso common:is_tweets_t(Tweets, UserId)
                              andalso I == UserId + 1
                              andalso sets:is_set(Subscriptions)
                              andalso lists:all(fun common:is_user_id_t/1,
                                                sets:to_list(Subscriptions))
                      catch
                          _Exception:_Reason -> false
                      end
              end,
              lists:zip(lists:seq(1, length(X)), X));
        true ->
            false
    end.
