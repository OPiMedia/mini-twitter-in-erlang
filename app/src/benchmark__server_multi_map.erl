%%% Simple benchmarks used during development.
%%%
%%% @author Olivier Pirson [http://www.opimedia.be/]
%%% @version April 21, 2018
-module(benchmark__server_multi_map).

-include_lib("config.hrl").
-include_lib("debug.hrl").

-include_lib("stdlib/include/assert.hrl").

-export([test_get_timeline/0,
         test_get_tweets/0,
         test_tweet/0]).

-include("benchmark__config.hrl").



%% Benchmarks

%% Creates a server with 5000 users following 25 others and sending 10 tweets.
initialize_server_() ->
    rand:seed_s(exsplus, {0, 0, 0}),
    ServerPids = server_multi_map:initialize(),
    %% Register users
    NumberOfUsers = 5000,
    UserIds = lists:map(fun(I) ->
                                {UserId, _} =
                                    server:register_user(
                                      server_multi_map:i_to_data_actor_pid(I, ServerPids)),
                                ?assert(UserId == I),
                                UserId
                        end,
                        lists:seq(0, NumberOfUsers - 1)),
    %% Create their subscriptions
    NumberOfSubscriptions = 25,
    lists:foreach
      (fun(UserId) ->
               lists:foreach(fun(_) ->
                                     ok =
                                         server:subscribe(
                                           server_multi_map:i_to_data_actor_pid(UserId, ServerPids),
                                           UserId, benchmark:pick_random(UserIds))
                                     %% It can happen that we randomly pick a user which we're
                                     %% already subscribed to. In that case the # subscriptions
                                     %% for this user is < NumberOfSubscriptions.
                             end,
                             lists:seq(1, NumberOfSubscriptions))
       end,
       UserIds),
    %% Add some tweets
    NumberOfTweets = 10,
    lists:foreach
      (fun(UserId) ->
               lists:foreach(fun(_) ->
                                     _Timestamp =
                                         server:tweet(
                                           server_multi_map:i_to_data_actor_pid(UserId, ServerPids),
                                           UserId, "Hello!")
                             end,
                             lists:seq(1, NumberOfTweets))
       end,
       UserIds),
    {ServerPids, UserIds}.


%% Get timeline of 10000 users (repeated ?NB_REPEAT times).
test_get_timeline() ->
    io:fwrite("test_get_timeline --- initialize_server_~n"),
    {ServerPids, UserIds} = initialize_server_(),
    io:fwrite("run_benchmark~n"),
    benchmark:run_benchmark
      ("get_timeline",
       fun() ->
               lists:foreach(fun(_) ->
                                     UserId = benchmark:pick_random(UserIds),
                                     server:get_timeline(
                                       server_multi_map:i_to_data_actor_pid(UserId, ServerPids),
                                       UserId, 1)
                             end,
                             lists:seq(1, 10000))
       end,
       ?NB_REPEAT).


%% Get tweets of 10000 users (repeated ?NB_REPEAT times).
test_get_tweets() ->
    io:fwrite("test_get_tweets --- initialize_server_~n"),
    {ServerPids, UserIds} = initialize_server_(),
    io:fwrite("run_benchmark~n"),
    benchmark:run_benchmark
      ("get_tweets",
       fun() ->
               lists:foreach(fun(_) ->
                                     UserId = benchmark:pick_random(UserIds),
                                     server:get_tweets(
                                       server_multi_map:i_to_data_actor_pid(UserId, ServerPids),
                                       UserId, 1)
                             end,
                             lists:seq(1, 10000))
       end,
       ?NB_REPEAT).


%% Tweet for 1000 users (repeated ?NB_REPEAT times).
test_tweet() ->
    io:fwrite("test_tweet --- initialize_server --- ~B scheduler(s) --- ~B data actor(s)~n",
              [erlang:system_info(schedulers), ?NB_DATA_ACTOR]),
    {ServerPids, UserIds} = initialize_server_(),
    io:fwrite("run_benchmark~n"),
    benchmark:run_benchmark
      ("tweet",
       fun() ->
               lists:foreach(fun(_) ->
                                     UserId = benchmark:pick_random(UserIds),
                                     server:tweet(
                                       server_multi_map:i_to_data_actor_pid(UserId, ServerPids),
                                       UserId, "Test")
                             end,
                             lists:seq(1, 1000))
       end,
       ?NB_REPEAT).
