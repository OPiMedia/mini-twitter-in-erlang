%%% @doc Unit tests for common.erl.
%%%
%%% @author Olivier Pirson [http://www.opimedia.be/]
%%% @copyright GPLv3 --- 2018 Olivier Pirson
%%% @version May 5, 2018

-module(test__common).

-include_lib("config.hrl").
-include_lib("debug.hrl").
-include_lib("test.hrl").

-include_lib("eunit/include/eunit.hrl").



starting_msg_info_test() ->
    ct:print("START tests in ~s~n", [?FILE]).



current_time__test() ->
    ?NOT_COVERED.


get_tweet_time__test() ->
    {TweetA, TweetB} = test:two_tweet(42, 666, "A", "A"),

    {_tweetA, _UserIdA, TimestampA, _TweetContentA} = TweetA,
    ?assertEqual(TimestampA, common:get_tweet_time(TweetA)),

    {_tweetB, _UserIdB, TimestampB, _TweetContentB} = TweetB,
    ?assertEqual(TimestampB, common:get_tweet_time(TweetB)).


is_ordonned_tweet__test() ->
    {TweetA, TweetB} = test:two_tweet(42, 666, "A", "A"),
    ?assert(not common:is_ordonned_tweet(TweetA, TweetB)),
    ?assert(common:is_ordonned_tweet(TweetB, TweetA)),

    {TweetC, TweetD} = test:two_tweet(42, 666, "C", "D"),
    ?assert(not common:is_ordonned_tweet(TweetC, TweetD)),
    ?assert(common:is_ordonned_tweet(TweetD, TweetC)).


is_page_t__test() ->
    ?assert(common:is_page_t(1)),
    ?assert(common:is_page_t(2)),

    ?assertNot(common:is_page_t(0)),
    ?assertNot(common:is_page_t(1.0)),
    ?assertNot(common:is_page_t([])).


is_tweet_t__test() ->
    ?assert(common:is_tweet_t({tweet, 0, 0, "content"})),

    ?assertNot(common:is_tweet_t({zzz, 0, 0, "content"})),
    ?assertNot(common:is_tweet_t({tweet, -1, 0, "content"})),
    ?assertNot(common:is_tweet_t({tweet, 0, -1, "content"})),
    ?assertNot(common:is_tweet_t({tweet, 0, 0, ""})),
    ?assertNot(common:is_tweet_t({tweet})),
    ?assertNot(common:is_tweet_t([])).


is_tweet_t_2__test() ->
    ?assert(common:is_tweet_t({tweet, 0, 0, "content"}, 0)),
    ?assert(common:is_tweet_t({tweet, 1, 0, "content"}, 1)),

    ?assertNot(common:is_tweet_t({tweet, 0, 0, "content"}, 1)),
    ?assertNot(common:is_tweet_t({tweet, 1, 0, "content"}, 0)),
    ?assertNot(common:is_tweet_t({zzz, 0, 0, "content"}, 0)),
    ?assertNot(common:is_tweet_t({tweet, -1, 0, "content"}, 0)),
    ?assertNot(common:is_tweet_t({tweet, 0, -1, "content"}, 0)),
    ?assertNot(common:is_tweet_t({tweet, 0, 0, ""}, 0)),
    ?assertNot(common:is_tweet_t({tweet}, 0)),
    ?assertNot(common:is_tweet_t([], 0)).


is_tweets_t__test() ->
    ?assert(common:is_tweets_t([])),

    {TweetA, TweetB} = test:two_tweet(42, 666, "TweetA", "TweetB"),
    ?assert(common:is_tweets_t([TweetA])),
    ?assert(common:is_tweets_t([TweetB])),
    ?assert(common:is_tweets_t([TweetA, TweetB])),

    ?assertNot(common:is_tweets_t([42])),
    ?assertNot(common:is_tweets_t("blabla")).


is_tweets_t_2__test() ->
    ?assert(common:is_tweets_t([], 123)),

    {TweetA, TweetB} = test:two_tweet(42, 666, "TweetA", "TweetB"),
    ?assert(common:is_tweets_t([TweetA], 42)),
    ?assert(common:is_tweets_t([TweetB], 666)),

    ?assertNot(common:is_tweets_t([TweetA, TweetB], 42)),
    ?assertNot(common:is_tweets_t([TweetA, TweetB], 666)),
    ?assertNot(common:is_tweets_t([42], 0)),
    ?assertNot(common:is_tweets_t("blabla", 0)).


is_tweet_time_t__test() ->
    ?assert(common:is_tweet_time_t(0)),
    ?assert(common:is_tweet_time_t(1)),
    ?assert(common:is_tweet_time_t(2)),

    ?assertNot(common:is_tweet_time_t(1.0)),
    ?assertNot(common:is_tweet_time_t([])).


is_tweet_content_t__test() ->
    ?assert(common:is_tweet_content_t("content")),

    ?assertNot(common:is_tweet_content_t("")),
    ?assertNot(common:is_tweet_content_t([])),
    ?assertNot(common:is_tweet_content_t(42)),
    ?assertNot(common:is_tweet_content_t([-42])).


is_user_id_t__test() ->
    ?assert(common:is_user_id_t(0)),
    ?assert(common:is_user_id_t(1)),
    ?assert(common:is_user_id_t(2)),

    ?assertNot(common:is_user_id_t(1.0)),
    ?assertNot(common:is_user_id_t([])).


list_page__test() ->
    ?assertEqual([], common:list_page([], 1)),

    L1 = lists:seq(1, ?PAGE_SIZE),
    ?assertEqual(L1, common:list_page(L1, 1)),
    ?assertEqual([], common:list_page(L1, 2)),

    L2 = lists:seq(1, ?PAGE_SIZE - 1),
    ?assertEqual(L2, common:list_page(L2, 1)),
    ?assertEqual([], common:list_page(L2, 2)),

    L3 = lists:seq(1, ?PAGE_SIZE + 1),
    ?assertEqual(lists:seq(1, ?PAGE_SIZE), common:list_page(L3, 1)),
    ?assertEqual([?PAGE_SIZE + 1], common:list_page(L3, 2)),
    ?assertEqual([], common:list_page(L3, 3)),

    lists:foreach(fun(N) ->
                          L = lists:seq(1, N),
                          if
                              N > ?PAGE_SIZE*3 ->
                                  ?assertEqual(lists:seq(1, ?PAGE_SIZE), common:list_page(L, 1)),
                                  ?assertEqual(lists:seq(?PAGE_SIZE + 1, ?PAGE_SIZE*2),
                                               common:list_page(L, 2)),
                                  ?assertEqual(lists:seq(?PAGE_SIZE*2 + 1, ?PAGE_SIZE*3),
                                               common:list_page(L, 3));
                              true -> ok
                          end,
                          Len = length(L),
                          Rem = Len rem ?PAGE_SIZE,
                          NbPage = Len div ?PAGE_SIZE + if
                                                            Rem == 0 -> 0;
                                                            true -> 1
                                                        end,
                          LastPageSize = if
                                             Rem == 0 -> ?PAGE_SIZE;
                                             true -> Rem
                                         end,
                          ?assertEqual(lists:seq(?PAGE_SIZE*(NbPage - 1) + 1,
                                                 ?PAGE_SIZE*(NbPage - 1) + LastPageSize),
                                       common:list_page(L, NbPage)),
                          ?assertEqual([], common:list_page(L, NbPage + 1))
                  end,
                  lists:seq(1, 5000)).


mod_get__test() ->
    ?assertEqual(42, common:mod_get(0, [42], 1)),
    ?assertEqual(42, common:mod_get(1, [42], 1)),
    ?assertEqual(42, common:mod_get(2, [42], 1)),
    ?assertEqual(42, common:mod_get(3, [42], 1)),

    ?assertEqual(42, common:mod_get(0, [42, 666], 2)),
    ?assertEqual(666, common:mod_get(1, [42, 666], 2)),
    ?assertEqual(42, common:mod_get(2, [42, 666], 2)),
    ?assertEqual(666, common:mod_get(3, [42, 666], 2)),

    ?assertEqual(42, common:mod_get(0, [42, 666, 0], 3)),
    ?assertEqual(666, common:mod_get(1, [42, 666, 0], 3)),
    ?assertEqual(0, common:mod_get(2, [42, 666, 0], 3)),
    ?assertEqual(42, common:mod_get(3, [42, 666, 0], 3)).


page_to_enough_nb__test() ->
    ?assertEqual(?PAGE_SIZE, common:page_to_enough_nb(1)),
    ?assertEqual(?PAGE_SIZE*2, common:page_to_enough_nb(2)),
    ?assertEqual(?PAGE_SIZE*3, common:page_to_enough_nb(3)).


page_to_nb_skip__test() ->
    ?assertEqual(0, common:page_to_nb_skip(1)),
    ?assertEqual(?PAGE_SIZE, common:page_to_nb_skip(2)),
    ?assertEqual(?PAGE_SIZE*2, common:page_to_nb_skip(3)).


sorted_tweets_limited_merge__test() ->
    ?assertEqual([], common:sorted_tweets_limited_merge([], [], 42)),

    {TweetsA, TweetsB} = test:two_1234_tweets(0, 1, ""),
    ?assertEqual(lists:sublist(TweetsA, 42), common:sorted_tweets_limited_merge(TweetsA, [], 42)),
    ?assertEqual(lists:sublist(TweetsA, 42), common:sorted_tweets_limited_merge([], TweetsA, 42)),

    Tweets =
        lists:merge(fun({_user, _UserIdA, TimestampA, _TweetContentA},
                        {_user, _UserIdB, TimestampB, _TweetContentB}) ->
                            TimestampA >= TimestampB
                    end,
                    TweetsA, TweetsB),
    Tweets4 = lists:sublist(Tweets, 42),
    ?assertEqual(Tweets4, common:sorted_tweets_limited_merge(TweetsA, TweetsB, 42)),
    ?assertEqual(Tweets4, common:sorted_tweets_limited_merge(TweetsB, TweetsA, 42)),

    Tweets5 = lists:sublist(Tweets, 43),
    ?assertEqual(Tweets5, common:sorted_tweets_limited_merge(TweetsA, TweetsB, 43)),
    ?assertEqual(Tweets5, common:sorted_tweets_limited_merge(TweetsB, TweetsA, 43)).


sorted_tweets_limited_merge_list__test() ->
    ?assertEqual([], common:sorted_tweets_limited_merge_list([[]], 42)),

    {TweetsA, TweetsB} = test:two_1234_tweets(0, 1, ""),

    ?assertEqual(lists:sublist(TweetsA, 42),
                 common:sorted_tweets_limited_merge_list([lists:sublist(TweetsA, 42)], 42)),
    ?assertEqual(lists:sublist(TweetsA, 43),
                 common:sorted_tweets_limited_merge_list([lists:sublist(TweetsA, 43)], 43)),

    Tweets =
        lists:merge(fun({_user, _UserIdA, TimestampA, _TweetContentA},
                        {_user, _UserIdB, TimestampB, _TweetContentB}) ->
                            TimestampA >= TimestampB
                    end,
                    TweetsA, TweetsB),

    ?assertEqual(lists:sublist(Tweets, 42),
                 common:sorted_tweets_limited_merge_list([lists:sublist(TweetsA, 42),
                                                          lists:sublist(TweetsB, 42)], 42)),
    ?assertEqual(lists:sublist(Tweets, 43),
                 common:sorted_tweets_limited_merge_list([lists:sublist(TweetsA, 43),
                                                          lists:sublist(TweetsB, 43)], 43)).


sorted_tweets_merge__test() ->
    ?assertEqual([], common:sorted_tweets_merge([], [])),

    {TweetsA, TweetsB} = test:two_1234_tweets(0, 1, ""),
    ?assertEqual(TweetsA, common:sorted_tweets_merge(TweetsA, [])),
    ?assertEqual(TweetsA, common:sorted_tweets_merge([], TweetsA)),

    Merged = common:sorted_tweets_merge(TweetsA, TweetsB),
    ?assertEqual(length(TweetsA) + length(TweetsB), length(Merged)),
    ?assertEqual(lists:sort(fun common:is_ordonned_tweet/2, Merged), Merged).


timestamp_to_datetime_string__test() ->
    ct:print(common:timestamp_to_datetime_string(common:current_time())).


tweet_to_string__test() ->
    {TweetA, TweetB} = test:two_tweet(42, 666, "TweetA", "TweetB"),
    ct:print("~s~s", [common:tweet_to_string(TweetA),
                      common:tweet_to_string(TweetB)]).


tweets_to_string__test() ->
    ?assertEqual("", common:tweets_to_string([])),

    {TweetA, TweetB} = test:two_tweet(42, 666, "TweetA", "TweetB"),
    ?assertEqual(common:tweet_to_string(TweetA), common:tweets_to_string([TweetA])),
    ?assertEqual(common:tweet_to_string(TweetB), common:tweets_to_string([TweetB])),

    ?assertEqual(string:concat(common:tweet_to_string(TweetA), common:tweet_to_string(TweetB)),
                 common:tweets_to_string([TweetA, TweetB])).
