%%% @doc Helpers for benchmarks.
%%%
%%% @author Olivier Pirson [http://www.opimedia.be/]
%%% @copyright GPLv3 --- 2018 Olivier Pirson
%%% @version May 8, 2018

-module(benchmark).

-export([pick_random/1,
         run_benchmark/3]).



%% @doc Pick a random element from a list.
pick_random(List) ->
    lists:nth(rand:uniform(length(List)), List).


%% @doc ???
run_benchmark(Name, Fun, Times) ->
    ThisPid = self(),
    {TotalWallClockTime, TotalCpuTime} =
        lists:foldl(fun(N, {TotalWallClockTime, TotalCpuTime}) ->
                            %% Recommendation: to make the test fair, each run executes in its own,
                            %% newly created Erlang process. Otherwise, if all tests run in the same
                            %% process, the later tests start out with larger heap sizes and
                            %% therefore probably do fewer garbage collections. Also consider
                            %% restarting the Erlang emulator between each test.
                            %% Source: http://erlang.org/doc/efficiency_guide/profiling.html
                            spawn_link(fun() ->
                                               {WallClockTime, CpuTime} = run_benchmark_once_(Name, Fun, N),
                                               ThisPid ! {done, WallClockTime, CpuTime}
                                       end),
                            receive
                                {done, WallClockTime, CpuTime} ->
                                    {TotalWallClockTime + WallClockTime, TotalCpuTime + CpuTime}
                            end
                    end, {0, 0}, lists:seq(1, Times)),
    io:fwrite("Finished benchmark\t~s\tAVG\tWall clock time/CPU time\t~.3f ms\t~.3f ms~n",
              [Name, TotalWallClockTime / (Times*1000.0), TotalCpuTime / Times]).


%% @doc ???
run_benchmark_once_(Name, Fun, N) ->
    io:fwrite("Running benchmark\t~s\t~B", [Name, N]),

    %% Start timers
    %% Tips:
    %% * Wall clock time measures the actual time spent on the benchmark.
    %%   I/O, swapping, and other activities in the operating system kernel are
    %%   included in the measurements. This can lead to larger variations.
    %%   os:timestamp() is more precise (microseconds) than
    %%   statistics(wall_clock) (milliseconds)
    %% * CPU time measures the actual time spent on this program, summed for all
    %%   threads. Time spent in the operating system kernel (such as swapping and
    %%   I/O) is not included. This leads to smaller variations but is
    %%   misleading.
    StartTime = os:timestamp(), % Wall clock time
    statistics(runtime),       % CPU time, summed for all threads

    %% Run
    Fun(),

    %% Get and print statistics
    %% Recommendation [1]:
    %% The granularity of both measurement types can be high. Therefore, ensure
    %% that each individual measurement lasts for at least several seconds.
    %% [1] http://erlang.org/doc/efficiency_guide/profiling.html
    WallClockTime = timer:now_diff(os:timestamp(), StartTime),
    {_, CpuTime} = statistics(runtime),
    io:fwrite("\tWall clock time/CPU time\t~.3f ms\t~B ms~n",
              [WallClockTime / 1000.0, CpuTime]),
    {WallClockTime, CpuTime}.
