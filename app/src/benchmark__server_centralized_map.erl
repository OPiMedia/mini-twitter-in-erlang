%%% Simple benchmarks of server_centralized_map used during development.
%%%
%%% @author Olivier Pirson [http://www.opimedia.be/]
%%% @copyright GPLv3 --- 2018 Olivier Pirson
%%% @version April 29, 2018
-module(benchmark__server_centralized_map).

-export([test_get_timeline/0,
         test_get_tweets/0,
         test_tweet/0]).

-include("benchmark__config.hrl").
-include("debug.hrl").



%% ???
%% Benchmarks

%% Creates a server with 5000 users following 25 others and sending 10 tweets.
initialize_server_() ->
    rand:seed_s(exsplus, {0, 0, 0}),
    ServerPid = server_centralized_map:initialize(),
    %% Register users
    NumberOfUsers = 5000,
    UserIds = lists:map(fun(_) ->
                                {UserId, _} = server:register_user(ServerPid),
                                UserId
                        end,
                        lists:seq(1, NumberOfUsers)),
    %% Create their subscriptions
    NumberOfSubscriptions = 25,
    lists:foreach
      (fun(UserId) ->
               lists:foreach(fun(_) ->
                                     ok = server:subscribe(ServerPid, UserId,
                                                           benchmark:pick_random(UserIds))
                                     %% It can happen that we randomly pick a user which we're
                                     %% already subscribed to. In that case the # subscriptions
                                     %% for this user is < NumberOfSubscriptions.
                             end,
                             lists:seq(1, NumberOfSubscriptions))
       end,
       UserIds),
    %% Add some tweets
    NumberOfTweets = 10,
    lists:foreach
      (fun(UserId) ->
               lists:foreach(fun(_) ->
                                     _Timestamp = server:tweet(ServerPid, UserId, "Hello!")
                             end,
                             lists:seq(1, NumberOfTweets))
       end,
       UserIds),
    {ServerPid, UserIds}.


%% Get timeline of 10000 users (repeated ?NB_REPEAT times).
test_get_timeline() ->
    io:fwrite("test_get_timeline --- initialize_server_~n"),
    {ServerPid, UserIds} = initialize_server_(),
    io:fwrite("run_benchmark~n"),
    benchmark:run_benchmark
      ("get_timeline",
       fun() ->
               lists:foreach(fun(_) ->
                                     server:get_timeline(ServerPid, benchmark:pick_random(UserIds), 1)
                             end,
                             lists:seq(1, 10000))
       end,
       ?NB_REPEAT).


%% Get tweets of 10000 users (repeated ?NB_REPEAT times).
test_get_tweets() ->
    io:fwrite("test_get_tweets --- initialize_server_~n"),
    {ServerPid, UserIds} = initialize_server_(),
    io:fwrite("run_benchmark~n"),
    benchmark:run_benchmark
      ("get_tweets",
       fun() ->
               lists:foreach(fun(_) ->
                                     server:get_tweets(ServerPid, benchmark:pick_random(UserIds), 1)
                             end,
                             lists:seq(1, 10000))
       end,
       ?NB_REPEAT).


%% Tweet for 1000 users (repeated ?NB_REPEAT times).
test_tweet() ->
    io:fwrite("test_tweet --- initialize_server_~n"),
    {ServerPid, UserIds} = initialize_server_(),
    io:fwrite("run_benchmark~n"),
    benchmark:run_benchmark
      ("tweet",
       fun() ->
               lists:foreach(fun(_) ->
                                     server:tweet(ServerPid, benchmark:pick_random(UserIds), "Test")
                             end,
                             lists:seq(1, 1000))
       end,
       ?NB_REPEAT).
