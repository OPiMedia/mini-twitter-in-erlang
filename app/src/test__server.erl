%%% @doc Helper functions to tests all server implementations.
%%%
%%% @author Olivier Pirson [http://www.opimedia.be/]
%%% @copyright GPLv3 --- 2018 Olivier Pirson
%%% @version May 15, 2018

-module(test__server).

-include_lib("eunit/include/eunit.hrl").

-include("config.hrl").
-include("debug.hrl").
-include("test.hrl").

-export([add_subscriptions/2,
         add_tweets/4,
         add_users/2,
         add_users_tweets/2]).



%%
%% Helpers
%%

%% @doc With Subscriptions a list [UserId, UserIdToSubscribeTo],
%% subscribes (follows) each UserId to UserIdToSubscribeTo.
%% ServerPid must be a server data_actor.
%% For some implementations, each UserId and UserIdToSubscribeTo must be valid user id.
-spec add_subscriptions(pid(), [{common:user_id_t(), common:user_id_t()}]) -> ok.
add_subscriptions(ServerPid, Subscriptions) ->
    ?assert(is_pid(ServerPid)),
    add_subscriptions_loop_(ServerPid, Subscriptions).

add_subscriptions_loop_(_ServerPid, []) ->
    ok;
add_subscriptions_loop_(ServerPid,
                        [{UserId, UserIdToSubscribeTo} | RemainSubscriptions]) ->
    ?assert(is_pid(ServerPid)),
    ?assert(common:is_user_id_t(UserId)),
    ?assert(common:is_user_id_t(UserIdToSubscribeTo)),
    Result = server:subscribe(ServerPid, UserId, UserIdToSubscribeTo),
    ?assertEqual(ok, Result),
    add_subscriptions_loop_(ServerPid, RemainSubscriptions).


%% @doc Submits Nb tweets (automatically numbered) for a user.
%% ServerPid must be a server data_actor.
%% For some implementations UserId must be valid user id.
%% Nb must be >= 1.
-spec add_tweets(pid(), common:user_id_t(), common:tweet_content_t(), pos_integer()) -> ok.
add_tweets(ServerPid, UserId, TweetContentPrefix, Nb) ->
    ?assert(is_pid(ServerPid)),
    ?assert(common:is_user_id_t(UserId)),
    ?assert(io_lib:char_list(TweetContentPrefix)),
    ?assert(is_integer(Nb)),
    ?assert(Nb >= 1),
    add_tweets_loop_(ServerPid, UserId, TweetContentPrefix, Nb, 0).

add_tweets_loop_(_ServerPid, _UserId, _TweetContentPrefix, 0, _I) ->
    ok;
add_tweets_loop_(ServerPid, UserId, TweetContentPrefix, Nb, I) ->
    ?assert(is_pid(ServerPid)),
    ?assert(common:is_user_id_t(UserId)),
    ?assert(io_lib:char_list(TweetContentPrefix)),
    ?assert(is_integer(Nb)),
    ?assert(Nb >= 1),
    ?assert(is_integer(I)),
    ?assert(I >= 0),
    Timestamp = server:tweet(ServerPid, UserId, lists:flatten(
                                                  io_lib:format("~s~B", [TweetContentPrefix, I]))),
    ?assert(common:is_tweet_time_t(Timestamp)),
    add_tweets_loop_(ServerPid, UserId, TweetContentPrefix, Nb - 1, I + 1).


%% @doc Registers Nb new users.
%% ServerPid must be a server data_actor.
%% Nb must be >= 1.
-spec add_users(pid(), non_neg_integer()) -> ok.
add_users(ServerPid, Nb) ->
    ?assert(is_pid(ServerPid)),
    ?assert(is_integer(Nb)),
    ?assert(Nb >= 1),
    lists:foreach(fun(I) ->
                          {UserId, ResponsePid} = server:register_user(ServerPid),
                          ?assert(common:is_user_id_t(UserId)),
                          ?assert(UserId >= I - 1),
                          ?assert(is_pid(ResponsePid)),
                          ?assertEqual(ServerPid, ResponsePid)
                  end,
                  lists:seq(1, Nb)),
    ok.


%% @doc For each NbTweet in the list, submits NbTweet tweets (automatically numbered).
%% ServerPid must be a server data_actor.
%% Each NbTweet must be >= 0.
-spec add_users_tweets(pid(), [non_neg_integer()]) -> ok.
add_users_tweets(_ServerPid, []) ->
    ok;
add_users_tweets(ServerPid, [NbTweet | RemainNbTweet]) ->
    ?assert(is_pid(ServerPid)),
    ?assert(is_integer(NbTweet)),
    ?assert(NbTweet >= 0),
    {NewUserId, NewResponsePid} = server:register_user(ServerPid),
    ?assert(common:is_user_id_t(NewUserId)),
    ?assert(is_pid(NewResponsePid)),
    ?assertEqual(ServerPid, NewResponsePid),
    if
        NbTweet >= 1 ->
            add_tweets(ServerPid, NewUserId,
                       lists:flatten(
                         io_lib:format("Tweet from user ~B: ", [NewUserId])),
                       NbTweet);
        true ->
            ok
    end,
    add_users_tweets(ServerPid, RemainNbTweet).
